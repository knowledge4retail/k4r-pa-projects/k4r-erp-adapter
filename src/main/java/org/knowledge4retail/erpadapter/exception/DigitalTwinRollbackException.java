package org.knowledge4retail.erpadapter.exception;

public class DigitalTwinRollbackException extends RuntimeException{

    public DigitalTwinRollbackException(Throwable cause) {
        super(cause);
    }

    public DigitalTwinRollbackException(String message, Throwable cause) {
        super(message, cause);
    }

}
