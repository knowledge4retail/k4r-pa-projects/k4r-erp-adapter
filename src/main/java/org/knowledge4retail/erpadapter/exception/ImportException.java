package org.knowledge4retail.erpadapter.exception;

import lombok.Getter;

@Getter
public class ImportException extends RuntimeException{

    private String logicalSystem;
    private Object payload;

    public ImportException(String message) {
        super(message);
    }

    public ImportException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImportException(String message, String logicalSystem, Object payload) {
        super(message);
        this.logicalSystem = logicalSystem;
        this.payload = payload;
    }

    public ImportException(String message, String logicalSystem, Object payload, Throwable cause) {
        super(message, cause);
        this.logicalSystem = logicalSystem;
        this.payload = payload;
    }

}
