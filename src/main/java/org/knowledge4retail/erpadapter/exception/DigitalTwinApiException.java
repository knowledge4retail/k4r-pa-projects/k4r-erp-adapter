package org.knowledge4retail.erpadapter.exception;

public class DigitalTwinApiException extends RuntimeException{

    public DigitalTwinApiException(String message) {
        super(message);
    }

    public DigitalTwinApiException(Throwable cause) {
        super(cause);
    }

    public DigitalTwinApiException(String message, Throwable cause) {
        super(message, cause);
    }

}
