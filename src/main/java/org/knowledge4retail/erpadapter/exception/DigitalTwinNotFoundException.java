package org.knowledge4retail.erpadapter.exception;

public class DigitalTwinNotFoundException extends DigitalTwinApiException{

    public DigitalTwinNotFoundException(String message) {
        super(message);
    }

    public DigitalTwinNotFoundException(Throwable cause) {
        super(cause);
    }

    public DigitalTwinNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
