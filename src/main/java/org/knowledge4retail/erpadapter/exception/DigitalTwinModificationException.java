package org.knowledge4retail.erpadapter.exception;

public class DigitalTwinModificationException extends Exception{

    private String type;
    private String sapParentEntityId;
    private String dtEntityKey;

    public DigitalTwinModificationException(String message) {
        super(message);
    }


    public DigitalTwinModificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public DigitalTwinModificationException(String message, Throwable cause, String type, String dtEntityKey) {
        super(message, cause);
        this.type = type;
        this.dtEntityKey = dtEntityKey;
    }

    public String getType() {
        return type;
    }

    public String getDtEntityKey() {
        return dtEntityKey;
    }

    public String getSapParentEntityId() {
        return sapParentEntityId;
    }
}
