package org.knowledge4retail.erpadapter.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "adapter_object_text")
public class AdapterObjectText {

    @EmbeddedId
    private AdapterObjectTextPK adapterObjectTextPK;

    @Column(name = "object_name")
    private String objectName;

    @Column(name = "description")
    private String description;

}
