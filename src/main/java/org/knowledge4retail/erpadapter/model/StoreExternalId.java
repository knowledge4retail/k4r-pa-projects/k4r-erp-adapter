package org.knowledge4retail.erpadapter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "store_ext_id")
@AllArgsConstructor
@NoArgsConstructor
public class StoreExternalId {

    @EmbeddedId
    private StoreExternalIdPK externalIdPK;

    @Column(name = "internal_store_id")
    private int internalStoreId;
}
