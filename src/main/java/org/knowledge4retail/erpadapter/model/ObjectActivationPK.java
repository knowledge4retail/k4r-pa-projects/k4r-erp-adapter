package org.knowledge4retail.erpadapter.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ObjectActivationPK implements Serializable {

    private String objectId;
    private String logicalSystemId;

    public ObjectActivationPK() {

    }

    public ObjectActivationPK(String objectId, String logicalSystemId) {
        this.objectId = objectId;
        this.logicalSystemId = logicalSystemId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectActivationPK that = (ObjectActivationPK) o;
        return Objects.equals(objectId, that.objectId) && Objects.equals(logicalSystemId, that.logicalSystemId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(objectId, logicalSystemId);
    }
}
