package org.knowledge4retail.erpadapter.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "customer_ext_id")
public class CustomerExternalId {

    @EmbeddedId
    private CustomerExternalIdPK customerExternalIdPK;

    @Column(name = "internal_customer_id")
    private String internalCustomerId;
}
