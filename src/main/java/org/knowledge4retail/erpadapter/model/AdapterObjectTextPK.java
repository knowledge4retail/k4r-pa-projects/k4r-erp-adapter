package org.knowledge4retail.erpadapter.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AdapterObjectTextPK implements Serializable {

    @Column(name = "adapter_object_id")
    private String adapterObjectId;
    @Column(name = "language")
    private String language;

    public AdapterObjectTextPK() {

    }

    public AdapterObjectTextPK(String adapterObjectId, String language) {
        this.adapterObjectId = adapterObjectId;
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdapterObjectTextPK that = (AdapterObjectTextPK) o;
        return Objects.equals(adapterObjectId, that.adapterObjectId) && Objects.equals(language, that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adapterObjectId, language);
    }
}
