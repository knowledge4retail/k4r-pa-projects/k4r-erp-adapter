package org.knowledge4retail.erpadapter.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ProductExternalIdPK implements Serializable {

    private String logicalSystemId;
    private String externalProductId;

    public ProductExternalIdPK() {

    }

    public ProductExternalIdPK(String logicalSystemId, String externalProductId) {
        this.logicalSystemId = logicalSystemId;
        this.externalProductId = externalProductId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductExternalIdPK that = (ProductExternalIdPK) o;
        return Objects.equals(logicalSystemId, that.logicalSystemId) && Objects.equals(externalProductId, that.externalProductId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(logicalSystemId, externalProductId);
    }
}
