package org.knowledge4retail.erpadapter.model;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "object_activation")
public class ObjectActivation {

    @EmbeddedId
    private ObjectActivationPK objectActivationPK;

    @ManyToOne
    @PrimaryKeyJoinColumn(name="object_id", referencedColumnName="logical_system_id")
    private LogicalSystem logicalSystem;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "id_generation")
    private String idGeneration;
}
