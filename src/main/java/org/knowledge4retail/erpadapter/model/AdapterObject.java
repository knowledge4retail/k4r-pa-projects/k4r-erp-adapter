package org.knowledge4retail.erpadapter.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "adapter_object")
public class AdapterObject {

    @Id
    @Column(name = "id")
    private String id;

    @OneToMany
    @JoinColumn(name = "adapterObjectTextPK")
    private List<AdapterObjectText> adapterObjectTexts;

}
