package org.knowledge4retail.erpadapter.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "product_ext_id")
public class ProductExternalId {

    @EmbeddedId
    private ProductExternalIdPK productExternalIdPK;

    @Column(name = "internal_product_id")
    private String internalProductId;

}
