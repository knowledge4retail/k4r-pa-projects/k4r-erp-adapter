package org.knowledge4retail.erpadapter.model;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CustomerExternalIdPK implements Serializable {

    private String logicalSystemId;
    private String externalCustomerId;

    public CustomerExternalIdPK() {

    }

    public CustomerExternalIdPK(String logicalSystemId, String externalCustomerId) {
        this.logicalSystemId = logicalSystemId;
        this.externalCustomerId = externalCustomerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerExternalIdPK that = (CustomerExternalIdPK) o;
        return Objects.equals(logicalSystemId, that.logicalSystemId) && Objects.equals(externalCustomerId, that.externalCustomerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(logicalSystemId, externalCustomerId);
    }
}
