package org.knowledge4retail.erpadapter.model;

import lombok.Builder;
import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@Embeddable
@Builder
public class StoreExternalIdPK implements Serializable {

    private String logicalSystemId;
    private String externalStoreId;

    public StoreExternalIdPK() {

    }

    public StoreExternalIdPK(String logicalSystemId, String externalStoreId) {
        this.logicalSystemId = logicalSystemId;
        this.externalStoreId = externalStoreId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoreExternalIdPK that = (StoreExternalIdPK) o;
        return Objects.equals(logicalSystemId, that.logicalSystemId) && Objects.equals(externalStoreId, that.externalStoreId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(logicalSystemId, externalStoreId);
    }
}
