package org.knowledge4retail.erpadapter.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "adapter_activation")
public class AdapterActivation implements Serializable {

    @Id
    @Column(name = "store_id")
    private String store_id;
    @ManyToOne
    @JoinColumn(name = "logical_system_id")
    private LogicalSystem logicalSystem;
    @Column(name = "is_active")
    private boolean isActive;

}
