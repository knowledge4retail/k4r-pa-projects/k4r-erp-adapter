package org.knowledge4retail.erpadapter.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "logical_system")
public class LogicalSystem implements Serializable {

    @Id
    @Column(name = "logical_system_id")
    private String logicalSystemId;
    @Column(name = "adapter_host")
    private String adapterHost;
    @Column(name = "system_type_id", nullable = false)
    private String systemTypeId;

}
