package org.knowledge4retail.erpadapter.repository;

import org.knowledge4retail.erpadapter.model.SystemType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemTypeRepository extends JpaRepository<SystemType, String> {
}
