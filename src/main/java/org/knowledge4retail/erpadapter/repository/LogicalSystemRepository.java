package org.knowledge4retail.erpadapter.repository;

import org.knowledge4retail.erpadapter.model.LogicalSystem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogicalSystemRepository extends JpaRepository<LogicalSystem, String> {
}
