package org.knowledge4retail.erpadapter.repository;

import org.knowledge4retail.erpadapter.model.AdapterActivation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdapterActivationRepository extends JpaRepository<AdapterActivation, String> {
}
