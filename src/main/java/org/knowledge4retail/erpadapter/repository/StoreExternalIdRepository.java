package org.knowledge4retail.erpadapter.repository;

import org.knowledge4retail.erpadapter.model.StoreExternalId;
import org.knowledge4retail.erpadapter.model.StoreExternalIdPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreExternalIdRepository extends JpaRepository<StoreExternalId, StoreExternalIdPK> {
}
