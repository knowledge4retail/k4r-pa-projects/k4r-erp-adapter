package org.knowledge4retail.erpadapter.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.exception.EntityAlreadyExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import javax.persistence.EntityNotFoundException;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {

    /**
     * Handles unknown Exception and server errors
     *
     * @param ex thrown exception
     * @param wr web request
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<DefaultError> unknownException(Exception ex, WebRequest wr) {
        log.error("Unable to handle {}", wr.getDescription(false), ex);

        return new ResponseEntity<>(new DefaultError(Exception.class.getSimpleName(), ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handles EntityAlreadyExistsException
     *
     * @param ex thrown exception
     * @param wr web request
     */
    @ExceptionHandler(EntityAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<DefaultError> entityAlreadyExistsException(Exception ex, WebRequest wr) {
        log.error("Unable to handle {}", wr.getDescription(false), ex);

        return new ResponseEntity<>(new DefaultError(EntityAlreadyExistsException.class.getSimpleName(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles unknown EntityAlreadyExistsException
     *
     * @param ex thrown exception
     * @param wr web request
     */
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<DefaultError> entityNotFoundException(Exception ex, WebRequest wr) {
        log.error("Unable to handle {}", wr.getDescription(false), ex);

        return new ResponseEntity<>(new DefaultError(EntityNotFoundException.class.getSimpleName(), ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static class DefaultError {
        private String type;
        private String message;
    }
}
