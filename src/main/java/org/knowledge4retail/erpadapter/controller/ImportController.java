package org.knowledge4retail.erpadapter.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportDespatchAdvisesRequestDto;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportOperationResult;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportProductRequestDto;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportStoreRequestDto;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchAdviceDto;
import org.knowledge4retail.erpadapter.service.ImportService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class ImportController {

    private final ImportService<ImportStoreRequestDto> importStoreService;
    private final ImportService<ImportProductRequestDto> importProductService;
    private final ImportService<DespatchAdviceDto> importDespatchAdviceService;

    public ImportController(ImportService<ImportStoreRequestDto> importStoreService,
                            ImportService<ImportProductRequestDto> importProductService,
                            ImportService<DespatchAdviceDto> importDespatchAdviceService) {
        this.importStoreService = importStoreService;
        this.importProductService = importProductService;
        this.importDespatchAdviceService = importDespatchAdviceService;
    }

    @Operation(
            summary = "Imports a list of store from a given logical system",
            responses = {
                    @ApiResponse(responseCode = "201", description = "List of stores successfully imported", content = @Content(schema = @Schema(implementation = ImportOperationResult.class))),
                    @ApiResponse(responseCode = "400", description = "Request Body validation error")
            }
    )
    @PostMapping("api/v0/import/logicalsystem/{logicalSystemId}/store")
    public ResponseEntity<ImportOperationResult> importStores(@PathVariable("logicalSystemId") String logicalSystemId,
                                                                        @RequestBody ImportStoreRequestDto importStoreRequestDto) {

        log.debug("Received a request to import a list of stores (size={})", importStoreRequestDto.getData().size());


        return new ResponseEntity<>(importStoreService.importData(logicalSystemId, importStoreRequestDto),
                HttpStatus.CREATED);
    }

    @Operation(
            summary = "Imports a list of products from a given logical system",
            responses = {
                    @ApiResponse(responseCode = "201", description = "List of products successfully imported", content = @Content(schema = @Schema(implementation = ImportOperationResult.class))),
                    @ApiResponse(responseCode = "400", description = "Request Body validation error")
            }
    )
    @PostMapping("api/v0/import/logicalsystem/{logicalSystemId}/product")
    public ResponseEntity<ImportOperationResult> importProducts(@PathVariable("logicalSystemId") String logicalSystemId,
                                                                         @RequestBody ImportProductRequestDto importProductRequestDto) {

        log.debug("Received a request to import a list of products (size={})", importProductRequestDto.getProducts().size());


        return new ResponseEntity<>(importProductService.importData(logicalSystemId, importProductRequestDto),
                HttpStatus.CREATED);
    }

    @Operation(
            summary = "Deletes a list of products of a given logical system",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Products successfully deleted", content = @Content(schema = @Schema(implementation = ImportOperationResult.class))),
                    @ApiResponse(responseCode = "400", description = "Request Body validation error")
            }
    )
    @DeleteMapping("api/v0/import/logicalsystem/{logicalSystemId}/product")
    public ResponseEntity<ImportOperationResult> deleteProducts(@PathVariable("logicalSystemId") String logicalSystemId,
                                                                @RequestBody List<String> productIds) {
        log.debug("Received a list of products to delete (size={})", productIds.size());

        return new ResponseEntity<>(importProductService.deleteData(logicalSystemId, productIds), HttpStatus.OK);
    }

    @Operation(
            summary = "Imports a list of despatch advices from a given logical system",
            responses = {
                    @ApiResponse(responseCode = "201", description = "List of despatch advices successfully imported", content = @Content(schema = @Schema(implementation = ImportOperationResult.class))),
                    @ApiResponse(responseCode = "400", description = "Request Body validation error")
            }
    )
    @PostMapping("api/v0/import/logicalsystem/{logicalSystemId}/despatchadvice")
    public ResponseEntity<ImportOperationResult> importDespatchAdvises(@PathVariable("logicalSystemId") String logicalSystemId,
                                                                       @RequestBody DespatchAdviceDto despatchAdviceDto) {

        log.debug("Received a request to import a despatch advises");


        return new ResponseEntity<>(importDespatchAdviceService.importData(logicalSystemId, despatchAdviceDto),
                HttpStatus.CREATED);
    }
}
