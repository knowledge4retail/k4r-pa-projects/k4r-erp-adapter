package org.knowledge4retail.erpadapter.controller;

import org.knowledge4retail.erpadapter.dto.LogicalSystemDto;
import org.knowledge4retail.erpadapter.dto.SystemTypeDto;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.knowledge4retail.erpadapter.service.db.SystemTypeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SystemController {

    private final LogicalSystemService logicalSystemService;
    private final SystemTypeService systemTypeService;

    public SystemController(LogicalSystemService logicalSystemService, SystemTypeService systemTypeService) {
        this.logicalSystemService = logicalSystemService;
        this.systemTypeService = systemTypeService;
    }

    @GetMapping("api/v0/system/logical")
    public ResponseEntity<List<LogicalSystemDto>> getAllLogicalSystems() {
        return new ResponseEntity<>(logicalSystemService.readAll(), HttpStatus.OK);
    }

    @GetMapping("api/v0/system/logical/{id}")
    public ResponseEntity<LogicalSystemDto> getAllLogicalSystems(@PathVariable("id") String id) {
        return new ResponseEntity<>(logicalSystemService.read(id), HttpStatus.OK);
    }

    @PostMapping("api/v0/system/logical")
    public ResponseEntity<LogicalSystemDto> createLogicalSystem(@RequestBody LogicalSystemDto logicalSystemDto) {
        return new ResponseEntity<>(logicalSystemService.create(logicalSystemDto), HttpStatus.CREATED);
    }

    @GetMapping("api/v0/system/type")
    public ResponseEntity<List<SystemTypeDto>> getAllSystemTypes() {
        return new ResponseEntity<>(systemTypeService.readAll(), HttpStatus.OK);
    }

    @GetMapping("api/v0/system/type/{id}")
    public ResponseEntity<SystemTypeDto> getSystemType(@PathVariable("id") String id) {
        return new ResponseEntity<>(systemTypeService.read(id), HttpStatus.OK);
    }

    @PostMapping("api/v0/system/type")
    public ResponseEntity<SystemTypeDto> createSystemType(@RequestBody SystemTypeDto systemTypeDto) {
        return new ResponseEntity<>(systemTypeService.create(systemTypeDto), HttpStatus.CREATED);
    }
}
