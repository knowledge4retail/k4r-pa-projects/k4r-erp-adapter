package org.knowledge4retail.erpadapter.controller;

import org.knowledge4retail.erpadapter.dto.AdapterActivationDto;
import org.knowledge4retail.erpadapter.service.db.AdapterActivationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdapterActivationController {

    private final AdapterActivationService adapterActivationService;

    public AdapterActivationController(AdapterActivationService adapterActivationService) {
        this.adapterActivationService = adapterActivationService;
    }

    @GetMapping("api/v0/adapter/activation")
    public ResponseEntity<List<AdapterActivationDto>> getAllAdapterActivations() {
        return new ResponseEntity<>(adapterActivationService.readAll(), HttpStatus.OK);
    }

    @GetMapping("api/v0/adapter/activation/{id}")
    public ResponseEntity<AdapterActivationDto> getAdapterActivation(@PathVariable("id") String id) {
        return new ResponseEntity<>(adapterActivationService.read(id), HttpStatus.OK);
    }

    @PostMapping("api/v0/adapter/activation")
    public ResponseEntity<AdapterActivationDto> createAdapterActivation(@RequestBody AdapterActivationDto adapterActivationDto) {
        return new ResponseEntity<>(adapterActivationService.create(adapterActivationDto), HttpStatus.CREATED);
    }
}
