package org.knowledge4retail.erpadapter.converter;

import org.knowledge4retail.erpadapter.dto.SystemTypeDto;
import org.knowledge4retail.erpadapter.model.SystemType;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SystemTypeConverter {

    SystemTypeConverter INSTANCE = Mappers.getMapper(SystemTypeConverter.class);

    SystemType dtoToSystemType(SystemTypeDto systemTypeDto);
    SystemTypeDto systemTypeToDto(SystemType systemType);
    List<SystemTypeDto> systemTypesToDtos(List<SystemType> systemTypes);

}
