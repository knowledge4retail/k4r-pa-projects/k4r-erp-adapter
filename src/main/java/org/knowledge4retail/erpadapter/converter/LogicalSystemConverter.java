package org.knowledge4retail.erpadapter.converter;

import org.knowledge4retail.erpadapter.dto.LogicalSystemDto;
import org.knowledge4retail.erpadapter.model.LogicalSystem;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LogicalSystemConverter {

    LogicalSystemConverter INSTANCE = Mappers.getMapper(LogicalSystemConverter.class);

    LogicalSystem dtoToLogicalSystem(LogicalSystemDto logicalSystemDto);
    LogicalSystemDto logicalSystemToDto(LogicalSystem logicalSystem);
    List<LogicalSystemDto> logicalSystemsToDtos(List<LogicalSystem> logicalSystems);

}
