package org.knowledge4retail.erpadapter.converter;

import org.knowledge4retail.erpadapter.dto.AdapterActivationDto;
import org.knowledge4retail.erpadapter.model.AdapterActivation;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AdapterActivationConverter {

    AdapterActivationConverter INSTANCE = Mappers.getMapper(AdapterActivationConverter.class);

    AdapterActivationDto adapterActionToDto(AdapterActivation adapterActivation);
    AdapterActivation dtoToAdapterActivation(AdapterActivationDto adapterActivationDto);
    List<AdapterActivationDto> adapterActivationsToDtos(List<AdapterActivation> adapterActivations);

}
