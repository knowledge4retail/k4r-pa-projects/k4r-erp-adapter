package org.knowledge4retail.erpadapter.converter;

import org.knowledge4retail.erpadapter.dto.StoreExternalIdDto;
import org.knowledge4retail.erpadapter.model.StoreExternalId;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface StoreExternalIdConverter {

    StoreExternalIdConverter INSTANCE = Mappers.getMapper(StoreExternalIdConverter.class);

    StoreExternalIdDto storeExternalIdToDto (StoreExternalId storeExternalId);
    StoreExternalId dtoToStoreExternalId(StoreExternalIdDto storeExternalIdDto);
    List<StoreExternalIdDto> storeExternalIdsToDtos(List<StoreExternalId> storeExternalIds);
    List<StoreExternalId> dtosToStoreExternalIds(List<StoreExternalIdDto> storeExternalIdDtos);
}
