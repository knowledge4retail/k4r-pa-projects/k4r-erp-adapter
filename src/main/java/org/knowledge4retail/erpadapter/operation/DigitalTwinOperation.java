package org.knowledge4retail.erpadapter.operation;

import org.knowledge4retail.erpadapter.exception.DigitalTwinModificationException;
import org.knowledge4retail.erpadapter.exception.DigitalTwinRollbackException;

import java.util.List;

/**
 * A DigitalTwinOperation describes an atomic create, update or delete operation against
 * the digital twin API. If the operation fails, a {@link DigitalTwinRollbackException}
 * is thrown and the operation should be rolled back.
 *
 * @param <T> type of digital twin entity
 * @param <K> type of key which uniquely identifies the digital twin
 */
public abstract class DigitalTwinOperation<T, K> {

    private ExecutionStatus executionStatus;
    private K entityKey;

    /**
     * Describes whether the DigitalTwinOperation has been executed or not.
     */
    protected enum ExecutionStatus {
        EXECUTED, NOT_EXECUTED;
    }

    /**
     * Performs the operation against the digital twin API. This method
     * should only be called by {@link DigitalTwinOperation#execute(Object...)}.
     *
     * @param params parameters needed to execute this operation
     * @return created, updated or deleted digital twin
     */
    protected abstract T executeOperation(Object... params);

    /**
     * Rolls back the operation. This method should only be called by
     * {@link DigitalTwinOperation#rollback()}.
     *
     * @param entityKey entityKey key of created, updated or deleted digital twin
     */
    protected abstract void rollbackOperation(K entityKey);

    public DigitalTwinOperation() {
        this.executionStatus = ExecutionStatus.NOT_EXECUTED;
    }

    /**
     * Adds itself to a provided list of DigitalTwinOperations. This list should be
     * used to roll back all DigitalTwinOperation that belong to one import operation.
     *
     * @param list list of operations
     * @return this for method chaining
     */
    public DigitalTwinOperation<T, K> addToExecutionList(List<DigitalTwinOperation<?, ?>> list) {
        list.add(this);

        return this;
    }

    /**
     * Performs the operation against the digital twin API
     *
     * @param params parameters needed to execute this operation
     * @return created, updated or deleted digital twin
     * @throws DigitalTwinModificationException if operation fails
     */
    public T execute(Object... params) throws DigitalTwinModificationException{
        try {
            return executeOperation(params);
        } catch(Exception ex) {
            throw new DigitalTwinModificationException(ex.getMessage(), ex, this.getClass().getSimpleName(),
                    String.valueOf(entityKey));
        }
    }

    /**
     * Only rolls back the operations if it has been executed.
     *
     * @throws DigitalTwinModificationException if the roll back fails.
     */
    public void rollback() throws DigitalTwinModificationException{
        if (executionStatus == ExecutionStatus.EXECUTED) {
            try {
                if (entityKey == null) {
                    throw new IllegalStateException("Entity key was not set by child class. DigitalTwinOperation " +
                            this.getClass().getSimpleName() + " was executed and cannot be rolled back!");
                }

                rollbackOperation(entityKey);
            } catch (Exception ex) {
                throw new DigitalTwinModificationException(ex.getMessage(), ex, this.getClass().getSimpleName(),
                        String.valueOf(entityKey));
            }
        }
    }

    protected void setExecutionStatus(ExecutionStatus executionStatus) {
        this.executionStatus = executionStatus;
    }

    protected void setEntityKey(K entityKey) {
        this.entityKey = entityKey;
    }
}
