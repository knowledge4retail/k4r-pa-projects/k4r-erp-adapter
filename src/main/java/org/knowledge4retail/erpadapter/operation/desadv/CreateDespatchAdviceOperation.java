package org.knowledge4retail.erpadapter.operation.desadv;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchAdviceDto;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.DespatchAdviceDTService;
import org.knowledge4retail.erpadapter.service.dt.StoreDTService;

@Slf4j
public class CreateDespatchAdviceOperation extends DigitalTwinOperation<DespatchAdviceDto, Integer> {

    private final DespatchAdviceDTService despatchAdviceDTService;
    private final StoreDTService storeDTService;

    public CreateDespatchAdviceOperation(DespatchAdviceDTService despatchAdviceDTService, StoreDTService storeDTService) {
        this.despatchAdviceDTService = despatchAdviceDTService;
        this.storeDTService = storeDTService;
    }

    @Override
    protected DespatchAdviceDto executeOperation(Object... params) {
        DespatchAdviceDto despatchAdviceDto = (DespatchAdviceDto) params[0];
        DespatchAdviceDto convertedDespatchAdvice = convertStoreId(despatchAdviceDto);

        return despatchAdviceDTService.createDespatchAdvice(convertedDespatchAdvice)
                .onSuccess(createdDespatchAdvice -> {
                    setExecutionStatus(ExecutionStatus.EXECUTED);
                    setEntityKey(createdDespatchAdvice.getId());
                }).getOrElseThrow(DigitalTwinApiException::new);
    }

    @Override
    protected void rollbackOperation(Integer entityKey) {
        log.info("Rolling back despatch advice {}", entityKey);

        despatchAdviceDTService.deleteDespatchAdvice(entityKey);
    }

    /**
     * Sets the DT store id by using the store number field.
     *
     * @param despatchAdviceDto
     * @return
     */
    private DespatchAdviceDto convertStoreId(DespatchAdviceDto despatchAdviceDto) {
        despatchAdviceDto.setStoreId(resolveStoreIdByStoreNumber(despatchAdviceDto.getStoreNumber()));
        despatchAdviceDto.setStoreNumber(null);

        return despatchAdviceDto;
    }

    private Integer resolveStoreIdByStoreNumber(String storeNumber) {
        return storeDTService.getStoreIdByStoreNumber(storeNumber)
                .getOrElseThrow(DigitalTwinApiException::new);
    }
}
