package org.knowledge4retail.erpadapter.operation.desadv;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLineItemDto;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.DespatchAdviceDTService;

@Slf4j
public class CreateDespatchLineItemOperation extends DigitalTwinOperation<DespatchLineItemDto, Integer> {

    private final DespatchAdviceDTService despatchAdviceDTService;

    public CreateDespatchLineItemOperation(DespatchAdviceDTService despatchAdviceDTService) {
        this.despatchAdviceDTService = despatchAdviceDTService;
    }

    @Override
    protected DespatchLineItemDto executeOperation(Object... params) {
        DespatchLineItemDto despatchLineItemDto = (DespatchLineItemDto) params[0];
        Integer despatchLogisticalUnitId = (Integer) params[1];

        despatchLineItemDto.setDespatchLogisticUnitId(despatchLogisticalUnitId);

        return despatchAdviceDTService.createDespatchLineItem(despatchLineItemDto)
                .onSuccess(createdDespatchLineItem -> {
                    setExecutionStatus(ExecutionStatus.EXECUTED);
                    setEntityKey(createdDespatchLineItem.getId());
                }).getOrElseThrow(DigitalTwinApiException::new);
    }

    @Override
    protected void rollbackOperation(Integer entityKey) {
        log.info("Rolling back despatch line item {}", entityKey);

        despatchAdviceDTService.deleteDespatchLineItem(entityKey);
    }
}
