package org.knowledge4retail.erpadapter.operation.desadv;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLogisticUnitDto;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.DespatchAdviceDTService;

@Slf4j
public class CreateDespatchLogisticalUnitOperation extends DigitalTwinOperation<DespatchLogisticUnitDto, Integer> {

    private final DespatchAdviceDTService despatchAdviceDTService;

    public CreateDespatchLogisticalUnitOperation(DespatchAdviceDTService despatchAdviceDTService) {
        this.despatchAdviceDTService = despatchAdviceDTService;
    }

    @Override
    protected DespatchLogisticUnitDto executeOperation(Object... params) {
        DespatchLogisticUnitDto despatchLogisticUnitDto = (DespatchLogisticUnitDto) params[0];
        Integer despatchId = (Integer) params[1];

        despatchLogisticUnitDto.setDespatchAdviceId(despatchId);

        return despatchAdviceDTService.createDespatchLogisticalUnit(despatchLogisticUnitDto)
                .onSuccess(createdDespatchLogisticalUnit -> {
                    setExecutionStatus(ExecutionStatus.EXECUTED);
                    setEntityKey(createdDespatchLogisticalUnit.getId());
                }).getOrElseThrow(DigitalTwinApiException::new);
    }

    @Override
    protected void rollbackOperation(Integer entityKey) {
        log.info("Rolling back despatch logistical unit {}", entityKey);

        despatchAdviceDTService.deleteDespatchLogisticalUnit(entityKey);
    }
}
