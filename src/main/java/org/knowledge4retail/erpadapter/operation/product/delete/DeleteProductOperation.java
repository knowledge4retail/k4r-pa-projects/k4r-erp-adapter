package org.knowledge4retail.erpadapter.operation.product.delete;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;

@Slf4j
public class DeleteProductOperation extends DigitalTwinOperation<Void, String> {

    private final ProductDTService productDTService;

    public DeleteProductOperation(ProductDTService productDTService) {
        this.productDTService = productDTService;
    }

    @Override
    protected Void executeOperation(Object... params) {
        String productId = (String) params[0];

        productDTService.deleteProduct(productId).getOrElseThrow(DigitalTwinApiException::new);

        setExecutionStatus(ExecutionStatus.EXECUTED);
        setEntityKey(productId);

        return null;
    }

    @Override
    protected void rollbackOperation(String entityKey) {
        log.info("Rolling back of delete operation currently not supported (Product id={})",
                entityKey);
    }
}
