package org.knowledge4retail.erpadapter.operation.product.create;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.product.ProductDto;
import org.knowledge4retail.erpadapter.dto.product.Product;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;


@Slf4j
public class CreateProductOperation extends DigitalTwinOperation<ProductDto, String> {

    private final ProductDTService productDTService;

    public CreateProductOperation(ProductDTService productDTService) {
        this.productDTService = productDTService;
    }

    @Override
    protected ProductDto executeOperation(Object... params) {
        Product product = (Product) params[0];
        Integer materialGroupId = (Integer) params[1];

        return productDTService.createProduct(convert(product, materialGroupId))
                .onSuccess(createdDto -> {
                    setExecutionStatus(ExecutionStatus.EXECUTED);
                    setEntityKey(createdDto.getId());
                })
                .getOrElseThrow(DigitalTwinApiException::new);
    }

    @Override
    protected void rollbackOperation(String entityKey) {
        log.info("Rolling back product '{}'", entityKey);

        productDTService.deleteProduct(entityKey);
    }

    private ProductDto convert(Product product, Integer materialGroupId) {
        return ProductDto.builder()
                .id(product.getProductID())
                .materialGroupId(materialGroupId)
                .productType(product.getProductType())
                .productBaseUnit(product.getProductBaseUnit())
                .build();
    }
}
