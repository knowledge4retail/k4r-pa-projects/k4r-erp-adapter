package org.knowledge4retail.erpadapter.operation.product.create;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.product.ProductUnitDto;
import org.knowledge4retail.erpadapter.dto.product.Product;
import org.knowledge4retail.erpadapter.dto.product.ProductGtin;
import org.knowledge4retail.erpadapter.dto.product.ProductUnit;
import org.knowledge4retail.erpadapter.exception.DigitalTwinModificationException;
import org.knowledge4retail.erpadapter.exception.DigitalTwinRollbackException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CreateProductUnitOperation extends DigitalTwinOperation<List<ProductUnitDto>, List<Integer>> {

    private final ProductDTService productDTService;

    public CreateProductUnitOperation(ProductDTService productDTService) {
        this.productDTService = productDTService;
    }

    @Override
    protected List<ProductUnitDto> executeOperation(Object... params) {
        Product product = (Product) params[0];
        String productId = (String) params[1];

        List<Integer> entityIds = new ArrayList<>();
        List<ProductUnitDto> productUnitDtos = convert(product.getProductUnit(), productId);
        List<ProductUnitDto> result = new ArrayList<>();

        for (ProductUnitDto productUnitDto : productUnitDtos) {
            ProductUnitDto createdProductUnitDto = productDTService.createProductUnit(productUnitDto)
                    .onSuccess(p -> {
                        entityIds.add(p.getId());

                        setExecutionStatus(ExecutionStatus.EXECUTED);
                        setEntityKey(entityIds);
                    })
                    .getOrElseThrow(error -> new RuntimeException(error));

            createdProductUnitDto.setGtins(productUnitDto.getGtins());
            result.add(createdProductUnitDto);
        }

        return result;
    }

    @Override
    protected void rollbackOperation(List<Integer> entityKeys) {
        entityKeys.forEach(entityKey -> {
            log.info("Rolling back product unit '{}'", entityKey);

            productDTService.deleteProductUnit(entityKey);
        });
    }

    private List<ProductUnitDto> convert(List<ProductUnit> productUnits, String productId) {
        return productUnits.stream()
                .map(productUnit -> ProductUnitDto.builder()
                        .productId(productId)
                        .unitCode(productUnit.getUnitCode())
                        .numeratorBaseUnit(Integer.parseInt(productUnit.getNumeratorBaseUnit()))
                        .denominatorBaseUnit(Integer.parseInt(productUnit.getDenominatorBaseUnit()))
                        .length(Double.parseDouble(productUnit.getLength()))
                        .width(Double.parseDouble(productUnit.getWidth()))
                        .height(Double.parseDouble(productUnit.getHeight()))
                        .dimensionUnit(resolveUnitSymbolToUnitId(productUnit.getDimensionUnit()))
                        .volume(Double.parseDouble(productUnit.getVolume()))
                        .volumeUnit(resolveUnitSymbolToUnitId(productUnit.getVolumeUnit()))
                        .netWeight(0d) //TODO: net weight is missing in IDOC
                        .grossWeight(Double.parseDouble(productUnit.getGrossWeight()))
                        .weightUnit(resolveUnitSymbolToUnitId(productUnit.getWeightUnit()))
                        .maxStackSize(0) //TODO: max stack size is missing in IDOC
                        .gtins(productUnit.getProductGtin()) // Not part of the DT model. Used for easier handling
                        .build())
                .collect(Collectors.toList());
    }

    private Integer resolveUnitSymbolToUnitId(String unitSymbol) throws DigitalTwinRollbackException {
        return productDTService.getUnitBySymbol(unitSymbol)
                .getOrElseThrow(DigitalTwinRollbackException::new)
                .getId();
    }
}
