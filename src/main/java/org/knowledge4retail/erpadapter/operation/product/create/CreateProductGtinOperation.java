package org.knowledge4retail.erpadapter.operation.product.create;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.product.ProductGtinDto;
import org.knowledge4retail.erpadapter.dto.dt.product.ProductUnitDto;
import org.knowledge4retail.erpadapter.dto.product.ProductGtin;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CreateProductGtinOperation extends DigitalTwinOperation<List<ProductGtinDto>, List<Integer>> {

    private final ProductDTService productDTService;

    public CreateProductGtinOperation(ProductDTService productDTService) {
        this.productDTService = productDTService;
    }

    @Override
    protected List<ProductGtinDto> executeOperation(Object... params) {
        List<ProductUnitDto> productUnitDtos = (List<ProductUnitDto>) params[0];

        List<Integer> entityKeys = new ArrayList<>();

        return productUnitDtos.stream()
                .map(productUnitDto -> productUnitDto.getGtins()
                        .stream()
                        .map(productGtin -> convert(productGtin, productUnitDto.getId()))
                        .collect(Collectors.toList()))
                .flatMap(Collection::stream)
                .map(productGtinDto -> productDTService.createProductGtin(productGtinDto)
                        .onSuccess(createdProductGtin -> {
                            entityKeys.add(createdProductGtin.getId());

                            setExecutionStatus(ExecutionStatus.EXECUTED);
                            setEntityKey(entityKeys);
                        })
                        .getOrElseThrow(DigitalTwinApiException::new))
                .collect(Collectors.toList());
    }

    @Override
    protected void rollbackOperation(List<Integer> entityKeys) {
        entityKeys.forEach(entityKey -> {
            log.info("Rolling back product gtin '{}'", entityKey);

            productDTService.deleteProductGtin(entityKey);
        });

    }

    public ProductGtinDto convert(ProductGtin productGtin, Integer productUnitId) {
        return ProductGtinDto.builder()
                .gtin(productGtin.getGtin())
                .productUnitId(productUnitId)
                .mainGtinFlag(productGtin.getMainGtin().equalsIgnoreCase("X"))
                .gtinType(productGtin.getGtinType())
                .build();
    }
}
