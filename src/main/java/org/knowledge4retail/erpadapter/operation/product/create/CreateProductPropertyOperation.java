package org.knowledge4retail.erpadapter.operation.product.create;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.product.ProductPropertyDto;
import org.knowledge4retail.erpadapter.dto.product.Product;
import org.knowledge4retail.erpadapter.dto.product.ProductAttribute;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;
import org.knowledge4retail.erpadapter.service.dt.StoreDTService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CreateProductPropertyOperation extends DigitalTwinOperation<List<ProductPropertyDto>, List<ProductPropertyDto>> {

    private final ProductDTService productDTService;
    private final StoreDTService storeDTService;

    public CreateProductPropertyOperation(ProductDTService productDTService, StoreDTService storeDTService) {
        this.productDTService = productDTService;
        this.storeDTService = storeDTService;
    }

    @Override
    protected List<ProductPropertyDto> executeOperation(Object... params) {
        Product product = (Product) params[0];

        List<ProductPropertyDto> createdProductProperties = new ArrayList<>();

        return product.getProductAttributes().stream()
                .map(productAttribute -> convert(productAttribute, product.getProductID()))
                .map(productPropertyDto -> productDTService.createProductProperty(productPropertyDto)
                        .onSuccess(createdProductProperty -> {
                            createdProductProperties.add(createdProductProperty);
                            setExecutionStatus(ExecutionStatus.EXECUTED);

                            setEntityKey(createdProductProperties);
                        })
                        .getOrElseThrow(DigitalTwinApiException::new))
                .collect(Collectors.toList());
    }

    @Override
    protected void rollbackOperation(List<ProductPropertyDto> entityKeys) {
        entityKeys.forEach(productPropertyDto -> {
            log.info("Rolling back product property '{}'", productPropertyDto.getId());

            productDTService.deleteProductProperty(productPropertyDto);
        });
    }

    private ProductPropertyDto convert(ProductAttribute productAttribute, String productId) {
        return ProductPropertyDto.builder()
                .productId(productId)
                .characteristicId(resolveProductCharacteristicIdByCode(productAttribute.getCharacteristicCode()))
                .valueLow(productAttribute.getPropertyLow().isBlank() ? "0" : productAttribute.getPropertyLow()) //TODO: This may not be empty. Check with Max
                .valueHigh("0") //TODO: Value HIGH fehlt im IDOC
                .storeId(resolveStoreIdByStoreNumber(productAttribute.getStoreId()))
                .build();
    }

    private Integer resolveProductCharacteristicIdByCode(String code) {
        return productDTService.getProductCharacteristicByCode(code)
                .getOrElseThrow(DigitalTwinApiException::new)
                .getId();
    }

    private Integer resolveStoreIdByStoreNumber(String storeNumber) {
        return storeDTService.getStoreIdByStoreNumber(storeNumber)
                .getOrElseThrow(DigitalTwinApiException::new);
    }
}
