package org.knowledge4retail.erpadapter.operation.product.create;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.product.ProductDescriptionDto;
import org.knowledge4retail.erpadapter.dto.product.Product;
import org.knowledge4retail.erpadapter.dto.product.ProductDescription;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CreateProductDescriptionOperation extends DigitalTwinOperation<List<ProductDescriptionDto>, List<Integer>> {

    private final ProductDTService productDTService;

    public CreateProductDescriptionOperation(ProductDTService productDTService) {
        this.productDTService = productDTService;
    }

    @Override
    protected List<ProductDescriptionDto> executeOperation(Object... params) {
        Product product = (Product) params[0];

        List<Integer> entityIds = new ArrayList<>();

        return product.getProductDescription().stream()
                .map(productDescription -> convert(productDescription, product.getProductID()))
                .map(productDescriptionDto -> productDTService.createProductDescription(productDescriptionDto)
                        .onSuccess(createdProductDescription -> {
                           entityIds.add(createdProductDescription.getId());
                           setExecutionStatus(ExecutionStatus.EXECUTED);

                           setEntityKey(entityIds);
                        }).getOrElseThrow(DigitalTwinApiException::new))
                .collect(Collectors.toList());
    }

    @Override
    protected void rollbackOperation(List<Integer> entityKeys) {
        entityKeys.forEach(entityKey -> {
            log.info("Rolling back product description '{}'", entityKey);

            productDTService.deleteProductDescription(entityKey);
        });
    }

    private ProductDescriptionDto convert(ProductDescription productDescription, String productId) {
        return ProductDescriptionDto.builder()
                .productId(productId)
                .description(productDescription.getDescription())
                .isoLanguageCode(productDescription.getIsoCode())
                .build();
    }


}
