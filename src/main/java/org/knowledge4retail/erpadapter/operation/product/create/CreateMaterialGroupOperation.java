package org.knowledge4retail.erpadapter.operation.product.create;

import org.knowledge4retail.erpadapter.dto.dt.product.MaterialGroupDto;
import org.knowledge4retail.erpadapter.dto.product.Product;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;

public class CreateMaterialGroupOperation extends DigitalTwinOperation<MaterialGroupDto, Integer> {

    private final ProductDTService productDTService;

    public CreateMaterialGroupOperation(ProductDTService productDTService) {
        this.productDTService = productDTService;
    }

    @Override
    protected MaterialGroupDto executeOperation(Object... params) {
        Product param = (Product) params[0];
        //TODO: Create Material Group?

        return productDTService.getMaterialGroupByName(param.getMaterialGroup())
                .onSuccess(materialGroupDto -> {
                    setExecutionStatus(ExecutionStatus.EXECUTED);
                    setEntityKey(materialGroupDto.getId());
                })
                .getOrElseThrow(DigitalTwinApiException::new);
    }

    @Override
    protected void rollbackOperation(Integer entityKey) {

    }
}
