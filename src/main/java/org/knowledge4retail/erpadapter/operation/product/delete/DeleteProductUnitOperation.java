package org.knowledge4retail.erpadapter.operation.product.delete;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DeleteProductUnitOperation extends DigitalTwinOperation<Void, List<Integer>> {

    private final ProductDTService productDTService;

    public DeleteProductUnitOperation(ProductDTService productDTService) {
        this.productDTService = productDTService;
    }

    @Override
    protected Void executeOperation(Object... params) {
        List<Integer> idsToDelete = (List<Integer>) params[0];

        List<Integer> deletedIds = new ArrayList<>();

        for(Integer id : idsToDelete) {
            productDTService.deleteProductUnit(id).getOrElseThrow(DigitalTwinApiException::new);

            deletedIds.add(id);
            setExecutionStatus(ExecutionStatus.EXECUTED);

            setEntityKey(deletedIds);
        }

        return null;
    }

    @Override
    protected void rollbackOperation(List<Integer> entityKeys) {
        log.info("Rolling back of delete operation currently not supported (ProductUnit ids={})",
                entityKeys.toString());

    }
}
