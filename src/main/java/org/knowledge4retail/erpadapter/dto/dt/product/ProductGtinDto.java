package org.knowledge4retail.erpadapter.dto.dt.product;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.dto.BasicDto;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductGtinDto implements BasicDto {

    @Schema(description = "PK.autoincrement")
    private Integer id;
    private String gtin;
    @Schema(description = "FK")
    private Integer productUnitId;
    private String gtinType;
    private Boolean mainGtinFlag;
}
