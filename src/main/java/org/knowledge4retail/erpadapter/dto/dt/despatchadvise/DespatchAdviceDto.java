package org.knowledge4retail.erpadapter.dto.dt.despatchadvise;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DespatchAdviceDto {

    private Integer id;
    private Integer storeId;
    private String storeNumber;
    private OffsetDateTime shipTime;
    private OffsetDateTime creationTime;
    private String senderQualifier;
    private Integer senderId;
    private String documentNumber;

    /*
    Not part of the official Digital Twin Data Model.
    This is added for easier handling of data import
     */
    private List<DespatchLogisticUnitDto> despatchLogisticalUnits;
}
