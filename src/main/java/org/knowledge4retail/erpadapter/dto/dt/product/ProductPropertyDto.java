package org.knowledge4retail.erpadapter.dto.dt.product;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.dto.BasicDto;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductPropertyDto implements BasicDto {

    @Schema(description = "PK.autoincrement")
    private Integer id;
    @Schema(required = true, description = "FK.notBlank")
    private String productId;
    @Schema(required = true, description = "FK.notNull")
    private Integer characteristicId;
    @Schema(description = "FK")
    private Integer storeId;
    @NotBlank
    @Schema(required = true, description = "notBlank")
    private String valueLow;
    private String valueHigh;
}
