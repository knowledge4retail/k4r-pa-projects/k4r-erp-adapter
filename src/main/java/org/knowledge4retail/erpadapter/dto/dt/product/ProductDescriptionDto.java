package org.knowledge4retail.erpadapter.dto.dt.product;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.dto.BasicDto;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDescriptionDto implements BasicDto {

    @Schema(description = "PK.autoincrement")
    private Integer id;
    @Schema(description = "FK")
    private String productId;
    private String description;
    private String isoLanguageCode;
}
