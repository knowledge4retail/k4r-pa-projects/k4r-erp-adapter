package org.knowledge4retail.erpadapter.dto.dt.despatchadvise;

import lombok.Data;


@Data
public class DespatchLineItemDto {

    private Integer id;
    private Integer despatchLogisticUnitId;
    private String productId;
    private String requestedProductId;
    private Integer lineItemNumber;
    private Integer despatchQuantity;

}
