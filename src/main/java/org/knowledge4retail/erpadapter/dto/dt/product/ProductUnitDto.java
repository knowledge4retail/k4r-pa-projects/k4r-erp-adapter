package org.knowledge4retail.erpadapter.dto.dt.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.dto.BasicDto;
import org.knowledge4retail.erpadapter.dto.product.ProductGtin;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductUnitDto implements BasicDto {

    @Schema(description = "PK.autoincrement")
    private Integer id;
    @Schema(description = "FK")
    private String productId;
    private String unitCode;
    private Integer numeratorBaseUnit;
    private Integer denominatorBaseUnit;
    @NotNull
    @Schema(required = true, description = "notNull")
    private Double length;
    @NotNull
    @Schema(required = true, description = "notNull")
    private Double width;
    @NotNull
    @Schema(required = true, description = "notNull")
    private Double height;
    @Schema(description = "FK")
    private Integer dimensionUnit;
    private Double volume;
    @Schema(description = "FK")
    private Integer volumeUnit;
    private Double netWeight;
    private Double grossWeight;
    @Schema(description = "FK")
    private Integer weightUnit;
    private Integer maxStackSize;

    /*
    Not part of the official Digital Twin Data Model.
    This is added for easier handling of data import
     */
    @JsonIgnore
    private List<ProductGtin> gtins;
}
