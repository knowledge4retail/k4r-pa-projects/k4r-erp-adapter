package org.knowledge4retail.erpadapter.dto.dt.despatchadvise;

import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;

@Data
public class DespatchLogisticUnitDto {

    private Integer id;
    private Integer parentId;
    private Integer despatchAdviceId;
    private String productId;
    private String packageTypeCode;
    private Integer logisticUnitId;
    private OffsetDateTime estimatedDelivery;

    /*
    Not part of the official Digital Twin Data Model.
    This is added for easier handling of data import
     */
    private List<DespatchLineItemDto> despatchLineItems;
}
