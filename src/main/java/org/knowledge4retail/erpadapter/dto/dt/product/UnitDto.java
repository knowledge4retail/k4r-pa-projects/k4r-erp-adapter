package org.knowledge4retail.erpadapter.dto.dt.product;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.knowledge4retail.erpadapter.dto.BasicDto;

@Data
public class UnitDto implements BasicDto {

    @Schema(description = "PK.autoincrement")
    private Integer id;
    private String name;
    private String type;
    private String symbol;
}
