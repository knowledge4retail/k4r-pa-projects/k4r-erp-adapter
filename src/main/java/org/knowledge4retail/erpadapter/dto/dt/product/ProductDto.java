package org.knowledge4retail.erpadapter.dto.dt.product;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.dto.BasicDto;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto implements BasicDto {

    @Schema(required = true, description = "notBlank")
    @NotBlank
    private String id;
    @Schema(description = "FK")
    private Integer materialGroupId;
    private String productType;
    private String productBaseUnit;
}
