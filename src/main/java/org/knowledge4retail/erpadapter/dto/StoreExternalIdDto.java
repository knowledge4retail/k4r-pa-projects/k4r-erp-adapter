package org.knowledge4retail.erpadapter.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.model.StoreExternalIdPK;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StoreExternalIdDto {

    private StoreExternalIdPK externalIdPK;
    private int internalStoreId;
}
