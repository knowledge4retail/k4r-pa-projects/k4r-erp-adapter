package org.knowledge4retail.erpadapter.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AdapterActivationDto {

    private String storeId;
    private String logicalSystemId;
    private boolean isActive;

}
