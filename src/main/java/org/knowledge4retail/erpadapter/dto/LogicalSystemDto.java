package org.knowledge4retail.erpadapter.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LogicalSystemDto {

    private String logicalSystemId;
    private String hostName;
    private String systemTypeId;

}
