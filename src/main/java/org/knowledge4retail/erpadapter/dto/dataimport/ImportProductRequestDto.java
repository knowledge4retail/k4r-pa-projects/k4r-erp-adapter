package org.knowledge4retail.erpadapter.dto.dataimport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.dto.product.Product;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportProductRequestDto {

    private List<Product> products;

}
