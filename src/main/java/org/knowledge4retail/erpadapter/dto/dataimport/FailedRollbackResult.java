package org.knowledge4retail.erpadapter.dto.dataimport;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FailedRollbackResult {

    private String id;
    private String type;
    private String entityKey;
    private String reason;

}
