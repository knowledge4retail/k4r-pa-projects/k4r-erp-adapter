package org.knowledge4retail.erpadapter.dto.dataimport;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImportResult {

    public enum Status {
        SUCCESSFUL, FAILED
    }

    private String entityId;
    private Status status;
    private FailedImportResult failedImportResult;

}
