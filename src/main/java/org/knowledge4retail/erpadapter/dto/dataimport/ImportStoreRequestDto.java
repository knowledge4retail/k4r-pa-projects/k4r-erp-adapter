package org.knowledge4retail.erpadapter.dto.dataimport;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.knowledge4retail.erpadapter.dto.StoreDto;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImportStoreRequestDto {

    @JsonProperty("stores")
    private List<ImportLine<StoreDto>> data;

}
