package org.knowledge4retail.erpadapter.dto.dataimport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchAdviceDto;
import org.knowledge4retail.erpadapter.dto.product.Product;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImportDespatchAdvisesRequestDto {

    private List<DespatchAdviceDto> despatchAdvices;

}
