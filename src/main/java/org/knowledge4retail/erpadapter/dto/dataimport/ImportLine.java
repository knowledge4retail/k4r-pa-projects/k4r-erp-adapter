package org.knowledge4retail.erpadapter.dto.dataimport;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImportLine<T> {

    private String operationCode;

    @JsonProperty("data")
    private T payload;

}
