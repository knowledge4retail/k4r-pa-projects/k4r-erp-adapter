package org.knowledge4retail.erpadapter.dto.dataimport;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImportOperationResult {

    public enum ImportStatus {
        SUCCESSFUL, COMPLETE_WITH_ERRORS, FAILED
    }

    private ImportStatus status;

    private Date completionDate;

    private long totalOperationsCount;

    private long successfulOperationsCount;

    private long failedOperationsCount;

    private long successfulRollbackCount;

    private long failedRollbackCount;

    private List<FailedImportResult> failedImports;

}
