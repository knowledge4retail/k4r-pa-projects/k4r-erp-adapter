package org.knowledge4retail.erpadapter.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StoreDto {

    private Integer id;

    private String storeName;

    private String storeNumber;

    private String addressCountry;

    private String addressState;

    private String addressCity;

    private String addressPostcode;

    private String addressStreet;

    private String addressStreetNumber;

    private String addressAdditional;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private String cadPlanId;

}
