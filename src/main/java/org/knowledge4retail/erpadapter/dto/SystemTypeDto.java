package org.knowledge4retail.erpadapter.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SystemTypeDto {

    private String id;
    private String name;
    private String description;
    private String adapterImplementation;
}
