package org.knowledge4retail.erpadapter;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@OpenAPIDefinition(
        info = @Info(title = "K4R ERP Adapter API", version = "1", description = "K4R generic ERP adapter API reference")
)
@SpringBootApplication
@ConfigurationPropertiesScan("org.knowledge4retail.erpadapter")
public class K4rErpAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(K4rErpAdapterApplication.class, args);
    }

}
