package org.knowledge4retail.erpadapter.service;

import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.StoreDto;
import org.knowledge4retail.erpadapter.dto.StoreExternalIdDto;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportLine;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportStoreRequestDto;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportOperationResult;
import org.knowledge4retail.erpadapter.exception.ImportException;
import org.knowledge4retail.erpadapter.model.StoreExternalIdPK;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.knowledge4retail.erpadapter.service.dt.StoreDTService;
import org.knowledge4retail.erpadapter.service.db.StoreExternalIdService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service("importStoreService")
public class ImportStoreService extends ImportService<ImportStoreRequestDto>{

    private final StoreDTService storeDTService;
    private final StoreExternalIdService storeExternalIdService;
    private final LogicalSystemService logicalSystemService;

    public ImportStoreService(StoreDTService storeDTService, StoreExternalIdService storeExternalIdService,
                              LogicalSystemService logicalSystemService) {
        this.storeDTService = storeDTService;
        this.storeExternalIdService = storeExternalIdService;
        this.logicalSystemService = logicalSystemService;
    }

    @Override
    public ImportOperationResult importData(String logicalSystemId, ImportStoreRequestDto importStoreRequest) {
        if (!logicalSystemService.exists(logicalSystemId)) {
            throw new EntityNotFoundException("A logical system with id "
                    + "'" + logicalSystemId + "'"
                    + " does not exist. No stores were imported.");
        }

        List<ImportLine<StoreDto>> storesToCreate = new ArrayList<>();
        List<ImportLine<StoreDto>> storesToUpdate = new ArrayList<>();
        List<ImportLine<StoreDto>> storesToDelete = new ArrayList<>();

        importStoreRequest.getData().forEach(storeToImport ->
                sortStoreByOperationCode(storeToImport, storesToCreate, storesToUpdate, storesToDelete));

        List<Try<?>> allOperations = new ArrayList<>();
        allOperations.addAll(createStores(logicalSystemId, storesToCreate));
        allOperations.addAll(updateStores(logicalSystemId, storesToUpdate));
        allOperations.addAll(deleteStores(logicalSystemId, storesToDelete));

        return buildResponse(allOperations);
    }

    @Override
    public ImportOperationResult deleteData(String logicalSystemId, List<String> idList) {
        return null;
    }

    private <Type> void sortStoreByOperationCode(ImportLine<Type> line, List<ImportLine<Type>> createList,
                                                 List<ImportLine<Type>> updateList,
                                                 List<ImportLine<Type>> deleteList) {
        switch (line.getOperationCode()) {
            case "C":
                createList.add(line);
                break;
            case "U":
                updateList.add(line);
                break;
            case "D":
                deleteList.add(line);
                break;
            default:
                throw new IllegalArgumentException("Unknown operationType " + line.getOperationCode());
        }
    }

    private List<Try<?>> createStores(String logicalSystemId, List<ImportLine<StoreDto>> storesToCreate) {
        //TODO: Change this to a single request, once the DT-API allows bulk creation
        List<Try<?>> createdStores = storesToCreate.stream()
                .map(storeDtoImportLine -> {
                    //Check if a PK for this store already exists
                    StoreExternalIdPK storeExternalIdPK = new StoreExternalIdPK(logicalSystemId,
                            storeDtoImportLine.getPayload().getStoreNumber());

                    if (storeExternalIdService.exists(storeExternalIdPK)) {
                        String errorMessage = String.format("Store with external id '%s' of logical system '%s' " +
                                        "has already been mapped and cannot be imported again (use 'operationType' " +
                                        "'U' for update, not 'C' for create)",
                                storeDtoImportLine.getPayload().getStoreNumber(), logicalSystemId);

                        log.error(errorMessage);

                        return Try.failure(new ImportException(errorMessage, logicalSystemId,
                                storeDtoImportLine));
                    }

                    return storeDTService.create(storeDtoImportLine.getPayload());
                })
                .collect(Collectors.toList());

        //Create a mapping entry for each created store
        // TODO: Use mapstruct to convert DTOs to JPA entities?
        List<StoreExternalIdDto> storeExternalIdDtos = createdStores.stream()
                .filter(Try::isSuccess)
                .map((createdStore -> (StoreDto) createdStore.get()))
                .map(storeDto -> StoreExternalIdDto.builder()
                        .externalIdPK(new StoreExternalIdPK(logicalSystemId, storeDto.getStoreNumber()))
                        .internalStoreId(storeDto.getId())
                        .build())
                .collect(Collectors.toList());

        storeExternalIdService.createMany(storeExternalIdDtos);

        return createdStores;
    }

    private List<Try<?>> updateStores(String logicalSystemId, List<ImportLine<StoreDto>> storesToUpdate) {
        return storesToUpdate.stream()
                .map(storeDtoImportLine -> {
                    StoreDto updatedStoreDto = storeDtoImportLine.getPayload();

                    Try<StoreExternalIdDto> readTry = storeExternalIdService.read(
                            new StoreExternalIdPK(logicalSystemId, updatedStoreDto.getStoreNumber()));

                    if (readTry.isFailure()) {
                        Throwable cause = readTry.getCause();

                        return Try.failure(new ImportException(cause.getMessage(), logicalSystemId, storeDtoImportLine,
                                cause));
                    }

                    return storeDTService.update(readTry.get().getInternalStoreId(), updatedStoreDto);
                })
                .collect(Collectors.toList());
    }

    private List<Try<?>> deleteStores(String logicalSystemId, List<ImportLine<StoreDto>> storesToDelete) {
        return storesToDelete.stream()
                .map(storeDtoImportLine -> {
                    StoreDto updatedStoreDto = storeDtoImportLine.getPayload();

                    Try<StoreExternalIdDto> readTry = storeExternalIdService.read(
                            new StoreExternalIdPK(logicalSystemId, updatedStoreDto.getStoreNumber()));


                    if (readTry.isFailure())  {
                        Throwable cause = readTry.getCause();

                        return Try.failure(new ImportException(cause.getMessage(), logicalSystemId, storeDtoImportLine,
                                cause));
                    }

                    StoreExternalIdDto storeToDelete = readTry.get();

                    // Try to delete the DT of the store and return deleted store. If this was successful,
                    // delete the externalIdMapping as well.
                    return storeDTService
                            .delete(storeToDelete.getInternalStoreId())
                            .onSuccess(unused -> storeExternalIdService.delete(storeToDelete.getExternalIdPK()))
                            .recoverWith(cause -> Try.failure(new ImportException(cause.getMessage(), logicalSystemId
                                    , storeDtoImportLine, cause)));


                }).collect(Collectors.toList());
    }

    private ImportOperationResult buildResponse(List<Try<?>> allOperations) {
        List<StoreDto> successfulModifiedStores = new ArrayList<>();
        List<String> importStoreErrors = new ArrayList<>();

        ImportOperationResult.ImportStatus status = ImportOperationResult.ImportStatus.SUCCESSFUL;

        for (Try<?> tryStore : allOperations) {
            if (tryStore.isSuccess()) {
                successfulModifiedStores.add((StoreDto) tryStore.get());
            } else {
                Throwable cause = tryStore.getCause();

                importStoreErrors.add(cause.getMessage());
                status = ImportOperationResult.ImportStatus.COMPLETE_WITH_ERRORS;

                if (cause instanceof ImportException) {
                    ImportException importException = (ImportException) cause;
                    //TODO: What to do with this?
                }
            }
        }

        if (successfulModifiedStores.size() == 0) status = ImportOperationResult.ImportStatus.FAILED;

        return ImportOperationResult.<StoreDto>builder()
                .status(status)
                .successfulOperationsCount(successfulModifiedStores.size())
                .failedOperationsCount(importStoreErrors.size())
                .build();
    }
}
