package org.knowledge4retail.erpadapter.service;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dataimport.FailedImportResult;
import org.knowledge4retail.erpadapter.dto.dataimport.FailedRollbackResult;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportOperationResult;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportResult;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchAdviceDto;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLineItemDto;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLogisticUnitDto;
import org.knowledge4retail.erpadapter.exception.DigitalTwinModificationException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.operation.desadv.CreateDespatchAdviceOperation;
import org.knowledge4retail.erpadapter.operation.desadv.CreateDespatchLineItemOperation;
import org.knowledge4retail.erpadapter.operation.desadv.CreateDespatchLogisticalUnitOperation;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.knowledge4retail.erpadapter.service.dt.DespatchAdviceDTService;
import org.knowledge4retail.erpadapter.service.dt.StoreDTService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service("importDespatchAdviceService")
public class ImportDescpatchAdvicesService extends ImportService<DespatchAdviceDto> {

    private final DespatchAdviceDTService despatchAdviceDTService;
    private final StoreDTService storeDTService;

    private final LogicalSystemService logicalSystemService;

    public ImportDescpatchAdvicesService(DespatchAdviceDTService despatchAdviceDTService, StoreDTService storeDTService,
                                         LogicalSystemService logicalSystemService) {
        this.despatchAdviceDTService = despatchAdviceDTService;
        this.storeDTService = storeDTService;
        this.logicalSystemService = logicalSystemService;
    }

    @Override
    public ImportOperationResult importData(String logicalSystemId, DespatchAdviceDto data) {
        if (!logicalSystemService.exists(logicalSystemId)) {
            throw new EntityNotFoundException("A logical system with id "
                    + "'" + logicalSystemId + "'"
                    + " does not exist. No products were imported.");
        }

        ImportResult importResult = importDespatchAdvice(data);

        return computeResult(List.of(importResult), 1);
    }

    @Override
    public ImportOperationResult deleteData(String logicalSystemId, List<String> idList) {
        throw new UnsupportedOperationException("Delete despatch advices not implemented yet.");
    }

    private ImportResult importDespatchAdvice(DespatchAdviceDto despatchAdviceDto) {
        List<DigitalTwinOperation<?, ?>> executedOperations = new ArrayList<>();

        ImportResult importResult = new ImportResult();
        importResult.setEntityId(despatchAdviceDto.getDocumentNumber());
        importResult.setStatus(ImportResult.Status.SUCCESSFUL);

        try {
            DespatchAdviceDto createdDespatchAdvice = new CreateDespatchAdviceOperation(despatchAdviceDTService, storeDTService)
                    .addToExecutionList(executedOperations)
                    .execute(despatchAdviceDto);

            for(DespatchLogisticUnitDto despatchLogisticUnitDto : despatchAdviceDto.getDespatchLogisticalUnits()) {
                DespatchLogisticUnitDto createdDespatchUnit = new CreateDespatchLogisticalUnitOperation(despatchAdviceDTService)
                        .addToExecutionList(executedOperations)
                        .execute(despatchLogisticUnitDto, createdDespatchAdvice.getId());

                for(DespatchLineItemDto lineItemDto : despatchLogisticUnitDto.getDespatchLineItems()) {
                    new CreateDespatchLineItemOperation(despatchAdviceDTService)
                            .addToExecutionList(executedOperations)
                            .execute(lineItemDto, createdDespatchUnit.getId());
                }
            }

        } catch (DigitalTwinModificationException ex) {
            log.warn("Exception thrown while importing despatch advice '{}'. Rolling back despatch advice now...",
                    despatchAdviceDto.getDocumentNumber(), ex);

            FailedImportResult failedImportResult = new FailedImportResult();
            failedImportResult.setId(despatchAdviceDto.getDocumentNumber());
            failedImportResult.setMessage(ex.getMessage());

            // Loop through list in reverse in order to avoid fk key constraints while deleting entities...
            Collections.reverse(executedOperations);
            for (DigitalTwinOperation<?, ?> operation : executedOperations) {
                try {
                    operation.rollback();
                } catch (DigitalTwinModificationException exception) {
                    log.error("Unable to fully rollback product '{}': {}", exception.getDtEntityKey(),
                            exception.getMessage());

                    failedImportResult.setSuccessfulRollback(false);
                    failedImportResult.getFailedRollbackResults().add(FailedRollbackResult.builder()
                            .entityKey(exception.getDtEntityKey())
                            .id(despatchAdviceDto.getDocumentNumber())
                            .type(exception.getType())
                            .reason(exception.getMessage())
                            .build());
                }
            }

            log.info("Rollback of despatch advice '{}' complete.", despatchAdviceDto.getDocumentNumber());
            failedImportResult.setSuccessfulRollback(true);

            importResult.setFailedImportResult(failedImportResult);
            importResult.setStatus(ImportResult.Status.FAILED);
        }

        return importResult;

    }
}
