package org.knowledge4retail.erpadapter.service.db;

import org.knowledge4retail.erpadapter.dto.AdapterActivationDto;

import java.util.List;

public interface AdapterActivationService {

    List<AdapterActivationDto> readAll();

    AdapterActivationDto read(String id);

    AdapterActivationDto create(AdapterActivationDto adapterActivationDto);

    boolean exists(String id);

    boolean isActive(String id);

}
