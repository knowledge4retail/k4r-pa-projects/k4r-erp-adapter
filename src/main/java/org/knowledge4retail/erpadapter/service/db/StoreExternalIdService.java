package org.knowledge4retail.erpadapter.service.db;

import io.vavr.control.Try;
import org.knowledge4retail.erpadapter.dto.StoreExternalIdDto;
import org.knowledge4retail.erpadapter.model.StoreExternalIdPK;

import java.util.List;

public interface StoreExternalIdService {

    Try<StoreExternalIdDto> read(StoreExternalIdPK storeExternalIdPK);

    List<StoreExternalIdDto> readAll();

    StoreExternalIdDto create(StoreExternalIdDto storeExternalIdDto);

    List<StoreExternalIdDto> createMany(List<StoreExternalIdDto> storeExternalIdDtoList);

    void delete(StoreExternalIdPK storeExternalIdPK);

    void deleteAll();

    boolean exists(StoreExternalIdPK storeExternalIdPK);
}
