package org.knowledge4retail.erpadapter.service.db.impl;

import org.knowledge4retail.erpadapter.converter.AdapterActivationConverter;
import org.knowledge4retail.erpadapter.dto.AdapterActivationDto;
import org.knowledge4retail.erpadapter.exception.EntityAlreadyExistsException;
import org.knowledge4retail.erpadapter.model.AdapterActivation;
import org.knowledge4retail.erpadapter.repository.AdapterActivationRepository;
import org.knowledge4retail.erpadapter.service.db.AdapterActivationService;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class AdapterActivationServiceImpl implements AdapterActivationService {

    private final AdapterActivationRepository activationRepository;
    private final LogicalSystemService logicalSystemService;

    public AdapterActivationServiceImpl(AdapterActivationRepository activationRepository,
                                        LogicalSystemService logicalSystemService) {
        this.activationRepository = activationRepository;
        this.logicalSystemService = logicalSystemService;
    }

    @Override
    public List<AdapterActivationDto> readAll() {
        return AdapterActivationConverter.INSTANCE.adapterActivationsToDtos(activationRepository.findAll());
    }

    @Override
    public AdapterActivationDto read(String id) {
        return AdapterActivationConverter.INSTANCE.adapterActionToDto(activationRepository.getById(id));
    }

    @Override
    public AdapterActivationDto create(AdapterActivationDto adapterActivationDto) {
        if (activationRepository.existsById(adapterActivationDto.getStoreId())) {
            throw new EntityAlreadyExistsException("Logical system with id "
                    + adapterActivationDto.getLogicalSystemId()
                    + " already exists");
        }

        if (!logicalSystemService.exists(adapterActivationDto.getLogicalSystemId())) {
            throw new EntityNotFoundException("Logical system with id "
                    + adapterActivationDto.getLogicalSystemId()
                    + " does not exist");
        }

        AdapterActivation save =
                activationRepository.save(AdapterActivationConverter.INSTANCE.dtoToAdapterActivation(adapterActivationDto));

        return AdapterActivationConverter.INSTANCE.adapterActionToDto(save);
    }

    @Override
    public boolean exists(String id) {
        return false;
    }

    @Override
    public boolean isActive(String id) {
        Optional<AdapterActivation> optionalAdapterActivation = activationRepository.findById(id);

        return optionalAdapterActivation
                .map(AdapterActivation::isActive)
                .orElseThrow(() -> new EntityNotFoundException("Adapter with id " + id + " does not exist"));

    }
}
