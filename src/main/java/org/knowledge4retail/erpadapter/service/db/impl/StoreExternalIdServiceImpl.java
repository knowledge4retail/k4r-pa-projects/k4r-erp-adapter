package org.knowledge4retail.erpadapter.service.db.impl;

import io.vavr.control.Try;
import org.knowledge4retail.erpadapter.converter.StoreExternalIdConverter;
import org.knowledge4retail.erpadapter.dto.StoreExternalIdDto;
import org.knowledge4retail.erpadapter.exception.EntityAlreadyExistsException;
import org.knowledge4retail.erpadapter.model.StoreExternalId;
import org.knowledge4retail.erpadapter.model.StoreExternalIdPK;
import org.knowledge4retail.erpadapter.repository.StoreExternalIdRepository;
import org.knowledge4retail.erpadapter.service.db.StoreExternalIdService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreExternalIdServiceImpl implements StoreExternalIdService {

    private final StoreExternalIdRepository storeExternalIdRepository;

    public StoreExternalIdServiceImpl(StoreExternalIdRepository storeExternalIdRepository) {
        this.storeExternalIdRepository = storeExternalIdRepository;
    }

    @Override
    public Try<StoreExternalIdDto> read(StoreExternalIdPK storeExternalIdPK) {
        return Try.of(() -> StoreExternalIdConverter.INSTANCE.storeExternalIdToDto(storeExternalIdRepository.getById(storeExternalIdPK)));
    }

    @Override
    public List<StoreExternalIdDto> readAll() {
        return StoreExternalIdConverter.INSTANCE.storeExternalIdsToDtos(storeExternalIdRepository.findAll());
    }

    @Override
    public StoreExternalIdDto create(StoreExternalIdDto storeExternalIdDto) {
        if(storeExternalIdRepository.existsById(storeExternalIdDto.getExternalIdPK())) {
            throw new EntityAlreadyExistsException("An external store id mapping with key "
                    + storeExternalIdDto.getExternalIdPK().getLogicalSystemId()
                    + "/"
                    + storeExternalIdDto.getExternalIdPK().getExternalStoreId()
                    + " already exits.");
        }

        StoreExternalId save =
                storeExternalIdRepository.save(StoreExternalIdConverter.INSTANCE.dtoToStoreExternalId(storeExternalIdDto));

        return StoreExternalIdConverter.INSTANCE.storeExternalIdToDto(save);
    }

    @Override
    public List<StoreExternalIdDto> createMany(List<StoreExternalIdDto> storeExternalIdDtoList) {
        List<StoreExternalId> saveStoreExternalIds =
                storeExternalIdRepository.saveAll(StoreExternalIdConverter.INSTANCE.dtosToStoreExternalIds(storeExternalIdDtoList));

        return StoreExternalIdConverter.INSTANCE.storeExternalIdsToDtos(saveStoreExternalIds);
    }

    @Override
    public void delete(StoreExternalIdPK storeExternalIdPK) {
        storeExternalIdRepository.deleteById(storeExternalIdPK);
    }

    @Override
    public void deleteAll() {
        storeExternalIdRepository.deleteAll();
    }

    @Override
    public boolean exists(StoreExternalIdPK storeExternalIdPK) {
        return storeExternalIdRepository.existsById(storeExternalIdPK);
    }
}
