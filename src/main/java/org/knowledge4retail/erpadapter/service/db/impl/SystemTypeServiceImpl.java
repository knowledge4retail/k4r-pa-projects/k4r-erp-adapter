package org.knowledge4retail.erpadapter.service.db.impl;

import org.knowledge4retail.erpadapter.converter.SystemTypeConverter;
import org.knowledge4retail.erpadapter.dto.SystemTypeDto;
import org.knowledge4retail.erpadapter.exception.EntityAlreadyExistsException;
import org.knowledge4retail.erpadapter.model.SystemType;
import org.knowledge4retail.erpadapter.repository.SystemTypeRepository;
import org.knowledge4retail.erpadapter.service.db.SystemTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SystemTypeServiceImpl implements SystemTypeService {

    private final SystemTypeRepository systemTypeRepository;

    public SystemTypeServiceImpl(SystemTypeRepository systemTypeRepository) {
        this.systemTypeRepository = systemTypeRepository;
    }

    @Override
    public List<SystemTypeDto> readAll() {
        return SystemTypeConverter.INSTANCE.systemTypesToDtos(systemTypeRepository.findAll());
    }

    @Override
    public SystemTypeDto read(String id) {
        return SystemTypeConverter.INSTANCE.systemTypeToDto(systemTypeRepository.getById(id));
    }

    @Override
    public SystemTypeDto create(SystemTypeDto systemTypeDto) {
        if (systemTypeRepository.existsById(systemTypeDto.getId())) {
            throw new EntityAlreadyExistsException("System type with id "
                    + systemTypeDto.getId()
                    + " already exists");
        }

        SystemType save = systemTypeRepository.save(SystemTypeConverter.INSTANCE.dtoToSystemType(systemTypeDto));

        return SystemTypeConverter.INSTANCE.systemTypeToDto(save);
    }

    @Override
    public void delete(String id) {
        systemTypeRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        systemTypeRepository.deleteAll();
    }

    @Override
    public boolean exists(String id) {
        return systemTypeRepository.existsById(id);
    }
}
