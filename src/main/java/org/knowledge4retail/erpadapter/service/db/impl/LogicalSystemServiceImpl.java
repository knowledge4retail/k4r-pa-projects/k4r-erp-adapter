package org.knowledge4retail.erpadapter.service.db.impl;

import org.knowledge4retail.erpadapter.converter.LogicalSystemConverter;
import org.knowledge4retail.erpadapter.dto.LogicalSystemDto;
import org.knowledge4retail.erpadapter.exception.EntityAlreadyExistsException;
import org.knowledge4retail.erpadapter.model.LogicalSystem;
import org.knowledge4retail.erpadapter.repository.LogicalSystemRepository;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.knowledge4retail.erpadapter.service.db.SystemTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class LogicalSystemServiceImpl implements LogicalSystemService {

    private static final Logger logger = LoggerFactory.getLogger(LogicalSystemServiceImpl.class);

    private final LogicalSystemRepository logicalSystemRepository;
    private final SystemTypeService systemTypeService;

    public LogicalSystemServiceImpl(LogicalSystemRepository logicalSystemRepository,
                                    SystemTypeService systemTypeService) {
        this.logicalSystemRepository = logicalSystemRepository;
        this.systemTypeService = systemTypeService;
    }

    @Override
    public List<LogicalSystemDto> readAll() {
        return LogicalSystemConverter.INSTANCE.logicalSystemsToDtos(logicalSystemRepository.findAll());
    }

    @Override
    public LogicalSystemDto read(String id) {
        return LogicalSystemConverter.INSTANCE.logicalSystemToDto(logicalSystemRepository.getById(id));
    }

    @Override
    public LogicalSystemDto create(LogicalSystemDto logicalSystemDto) {

        if (logicalSystemRepository.existsById(logicalSystemDto.getLogicalSystemId())) {
            throw new EntityAlreadyExistsException("Logical system with id "
                    + logicalSystemDto.getLogicalSystemId()
                    + " already exists");
        }

        if (!systemTypeService.exists(logicalSystemDto.getSystemTypeId())) {
            throw new EntityNotFoundException("System type "
                    + logicalSystemDto.getSystemTypeId()
                    + " does not exist");
        }

        LogicalSystem logicalSystem = LogicalSystemConverter.INSTANCE.dtoToLogicalSystem(logicalSystemDto);
        LogicalSystem save = logicalSystemRepository.save(logicalSystem);

        return LogicalSystemConverter.INSTANCE.logicalSystemToDto(save);
    }

    @Override
    public void delete(String id) {
        logicalSystemRepository.deleteById(id);
    }


    @Override
    public void deleteAll() {
        logicalSystemRepository.deleteAll();
    }

    @Override
    public boolean exists(String id) {
        return logicalSystemRepository.existsById(id);
    }
}
