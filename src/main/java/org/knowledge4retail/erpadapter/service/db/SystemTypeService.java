package org.knowledge4retail.erpadapter.service.db;

import org.knowledge4retail.erpadapter.dto.SystemTypeDto;

import java.util.List;

public interface SystemTypeService {

    List<SystemTypeDto> readAll();

    SystemTypeDto read(String id);

    SystemTypeDto create(SystemTypeDto systemTypeDto);

    void delete(String id);

    void deleteAll();

    boolean exists(String id);
}
