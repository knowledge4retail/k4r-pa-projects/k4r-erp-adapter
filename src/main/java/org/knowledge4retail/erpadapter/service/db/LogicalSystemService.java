package org.knowledge4retail.erpadapter.service.db;

import org.knowledge4retail.erpadapter.dto.LogicalSystemDto;

import java.util.List;

public interface LogicalSystemService {

    List<LogicalSystemDto> readAll();

    LogicalSystemDto read(String id);

    LogicalSystemDto create(LogicalSystemDto logicalSystemDto);

    void delete(String id);

    void deleteAll();

    boolean exists(String id);
}
