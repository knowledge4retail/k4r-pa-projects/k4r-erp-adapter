package org.knowledge4retail.erpadapter.service.dt;

import io.vavr.control.Try;
import org.knowledge4retail.erpadapter.dto.StoreDto;

import java.util.List;

/**
 * Represents the K4R Digital Twin Store API
 */
public interface StoreDTService {

    StoreDto read(int id);

    List<StoreDto> readAll();

    Try<StoreDto> create(StoreDto storeDto);

    Try<StoreDto> update(int internalStoreId, StoreDto storeDto);

    Try<Void> delete(int internalStoreId);

    Try<Integer> getStoreIdByStoreNumber(String storeNumber);
}
