package org.knowledge4retail.erpadapter.service.dt.impl;

import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchAdviceDto;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLineItemDto;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLogisticUnitDto;
import org.knowledge4retail.erpadapter.service.dt.DespatchAdviceDTService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class DespatchAdviceDTServiceImpl implements DespatchAdviceDTService {

    private final RestTemplate k4rRestTemplate;
    private final GraphQLWebClient graphQLWebClient;

    @Value("${org.knowledge4retail.dt.baseUrl}")
    private String apiUrl;

    public DespatchAdviceDTServiceImpl(RestTemplate k4rRestTemplate, GraphQLWebClient graphQLWebClient) {
        this.k4rRestTemplate = k4rRestTemplate;
        this.graphQLWebClient = graphQLWebClient;
    }

    @Override
    public Try<DespatchAdviceDto> createDespatchAdvice(DespatchAdviceDto despatchAdviceDto) {
        log.debug("Create despatch advice '{}' via DT API", despatchAdviceDto);

        Integer storeId = despatchAdviceDto.getStoreId();

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/stores/" + storeId + "/despatchadvices", despatchAdviceDto, DespatchAdviceDto.class))
                .onSuccess(createdDespatchAdivce -> log.debug("Product successfully created: {}", createdDespatchAdivce))
                .onFailure(error -> log.error("Unable to create product '{}'", despatchAdviceDto.getDocumentNumber()));
    }

    @Override
    public Try<Object> deleteDespatchAdvice(Integer despatchAdviceId) {
        log.debug("Deleting despatch advice '{}' from DT API", despatchAdviceId);

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/despatchadvices/" + despatchAdviceId);
                    return null;
                })
                .onSuccess(nothing -> log.debug("Despatch advice '{}' successfully deleted", despatchAdviceId))
                .onFailure(error -> log.error("Unable to delete despatch advice '{}': {}", despatchAdviceId, error.getMessage(),
                        error));
    }

    @Override
    public Try<DespatchLogisticUnitDto> createDespatchLogisticalUnit(DespatchLogisticUnitDto despatchLogisticUnitDto) {
        log.debug("Create despatch logistical unit '{}' via DT API", despatchLogisticUnitDto);

        Integer despatchAdviceId = despatchLogisticUnitDto.getDespatchAdviceId();

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/stores/despatchadvices/" + despatchAdviceId + "/despatchlogisticunits", despatchLogisticUnitDto, DespatchLogisticUnitDto.class))
                .onSuccess(createdUnit -> log.debug("Despatch logistical unit successfully created: {}", createdUnit))
                .onFailure(error -> log.error("Unable to despatch logistical unit '{}'", despatchLogisticUnitDto.getLogisticUnitId()));
    }

    @Override
    public Try<Object> deleteDespatchLogisticalUnit(Integer despatchLogisticalUnitId) {
        log.debug("Deleting despatch logistical unit '{}' from DT API", despatchLogisticalUnitId);

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/despatchlogisticalunits/" + despatchLogisticalUnitId);
                    return null;
                })
                .onSuccess(nothing -> log.debug("Despatch logistical unit '{}' successfully deleted", despatchLogisticalUnitId))
                .onFailure(error -> log.error("Unable to delete despatch logistical unit '{}': {}", despatchLogisticalUnitId, error.getMessage(),
                        error));
    }

    @Override
    public Try<DespatchLineItemDto> createDespatchLineItem(DespatchLineItemDto despatchLineItemDto) {
        log.debug("Create despatch line item '{}' via DT API", despatchLineItemDto);

        Integer unitId = despatchLineItemDto.getDespatchLogisticUnitId();

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/stores/despatchlogisticalunits/" + unitId + "/despatchlineitems", despatchLineItemDto, DespatchLineItemDto.class))
                .onSuccess(createdItem -> log.debug("Despatch line item successfully created: {}", createdItem))
                .onFailure(error -> log.error("Unable to despatch line item '{}'", despatchLineItemDto.getLineItemNumber()));
    }

    @Override
    public Try<Object> deleteDespatchLineItem(Integer despatchLineItemId) {
        log.debug("Deleting despatch line item '{}' from DT API", despatchLineItemId);

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/despatchlineitems/" + despatchLineItemId);
                    return null;
                })
                .onSuccess(nothing -> log.debug("Despatch line item '{}' successfully deleted", despatchLineItemId))
                .onFailure(error -> log.error("Unable to delete despatch line item '{}': {}", despatchLineItemId, error.getMessage(),
                        error));
    }
}
