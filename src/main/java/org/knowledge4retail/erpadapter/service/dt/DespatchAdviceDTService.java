package org.knowledge4retail.erpadapter.service.dt;

import io.vavr.control.Try;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchAdviceDto;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLineItemDto;
import org.knowledge4retail.erpadapter.dto.dt.despatchadvise.DespatchLogisticUnitDto;

public interface DespatchAdviceDTService {

    Try<DespatchAdviceDto> createDespatchAdvice(DespatchAdviceDto despatchAdviceDto);

    Try<Object> deleteDespatchAdvice(Integer despatchAdviceId);

    Try<DespatchLogisticUnitDto> createDespatchLogisticalUnit(DespatchLogisticUnitDto despatchLogisticUnitDto);

    Try<Object> deleteDespatchLogisticalUnit(Integer despatchLogisticalUnitId);

    Try<DespatchLineItemDto> createDespatchLineItem(DespatchLineItemDto despatchLineItemDto);

    Try<Object> deleteDespatchLineItem(Integer despatchLineItemId);

}
