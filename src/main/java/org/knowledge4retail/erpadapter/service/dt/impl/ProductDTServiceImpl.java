package org.knowledge4retail.erpadapter.service.dt.impl;

import graphql.kickstart.spring.webclient.boot.GraphQLRequest;
import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dt.product.*;
import org.knowledge4retail.erpadapter.dto.product.Product;
import org.knowledge4retail.erpadapter.exception.DigitalTwinNotFoundException;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class ProductDTServiceImpl implements ProductDTService {

    private final RestTemplate k4rRestTemplate;
    private final GraphQLWebClient graphQLWebClient;

    @Value("${org.knowledge4retail.dt.baseUrl}")
    private String apiUrl;

    public ProductDTServiceImpl(RestTemplate k4rRestTemplate, GraphQLWebClient graphQLWebClient) {
        this.k4rRestTemplate = k4rRestTemplate;
        this.graphQLWebClient = graphQLWebClient;
    }

    @Override
    public Try<ProductDto> getProduct(String productId) {
        log.debug("Retrieving product '{}' from DT API", productId);

        return Try.of(() -> k4rRestTemplate.getForObject(apiUrl + "/products/" + productId, ProductDto.class))
                .onSuccess(productDto -> log.debug("Product successfully retrieved: {}", productDto))
                .onFailure(error -> log.error("Unable to retrieve product '{}'", productId));
    }

    @Override
    public Try<ProductDto> createProduct(ProductDto productDto) {
        log.debug("Create product '{}' via DT API", productDto);

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/products", productDto, ProductDto.class))
                .onSuccess(createdProductDto -> log.debug("Product successfully created: {}", createdProductDto))
                .onFailure(error -> log.error("Unable to create product '{}'", productDto.getId()));
    }

    @Override
    public Try<Object> deleteProduct(String productId) {
        log.debug("Deleting product '{}' from DT API", productId);

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/products/" + productId);
                    return null;
                })
                .onSuccess(nothing -> log.debug("Product '{}' successfully deleted", productId))
                .onFailure(error -> log.error("Unable to delete product '{}': {}", productId, error.getMessage(),
                        error));
    }

    @Override
    public Try<MaterialGroupDto> getMaterialGroupByName(String name) {
        return Try.of(() -> graphQLWebClient.post(
                                GraphQLRequest.builder().resource("graphql/get-materialgroup-by-name.graphql")
                                        .variables(Map.of("Name", name))
                                        .build())
                        .map(graphQLResponse -> graphQLResponse.getFirstList(MaterialGroupDto.class))
                        .map(materialGroups -> {
                            if (materialGroups.isEmpty())
                                throw new DigitalTwinNotFoundException("Material group with name '" + name +
                                        "' does not exist");

                            return materialGroups.get(0);
                        })
                        .block())
                .onSuccess(productGtin -> log.debug("Material group successfully retrieved: {}", productGtin))
                .onFailure(error -> log.error("Unable to retrieve material group : {}", error.getMessage(), error));
    }

    @Override
    public Try<UnitDto> getUnitBySymbol(String symbol) {
        return Try.of(() -> graphQLWebClient.post(
                                GraphQLRequest.builder().resource("graphql/get-unit-by-symbol.graphql")
                                        .variables(Map.of("Symbol", symbol))
                                        .build())
                        .map(graphQLResponse -> graphQLResponse.getFirstList(UnitDto.class))
                        .map(units -> {
                            if (units.isEmpty())
                                throw new DigitalTwinNotFoundException("Unit with symbol '" + symbol +
                                        "' does not exist");

                            return units.get(0);
                        })
                        .block())
                .onSuccess(unit -> log.debug("Unit successfully retrieved: {}", unit))
                .onFailure(error -> log.error("Unable to retrieve unit : {}", error.getMessage(), error));
    }

    @Override
    public Try<List<ProductUnitDto>> getAllProductUnitsByProductId(String productId) {
        log.debug("Retrieving product units by product id '{}'", productId);

        return Try.of(() -> graphQLWebClient.post(
                                GraphQLRequest.builder().resource("graphql/get-product-units-by-product-id.graphql")
                                        .variables(Map.of("ProductId", productId))
                                        .build())
                        .map(graphQLResponse -> graphQLResponse.getFirstList(ProductUnitDto.class))
                        .block())
                .onSuccess(productCharacteristicDto -> log.debug("Product units successfully retrieved: {}",
                        productCharacteristicDto))
                .onFailure(error -> log.error("Unable to retrieve units descriptions : {}", error.getMessage(), error));
    }

    @Override
    public Try<ProductUnitDto> createProductUnit(ProductUnitDto productUnitDto) {
        log.debug("Create product unit'{}' via DT API", productUnitDto);

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/productunits", productUnitDto,
                        ProductUnitDto.class))
                .onSuccess(createdProductDto -> log.debug("Product unit successfully created: {}", createdProductDto))
                .onFailure(error -> log.error("Unable to create product unit '{}'", productUnitDto.getId()));
    }

    @Override
    public Try<Object> deleteProductUnit(Integer productUnitId) {
        log.debug("Deleting product unit '{}' from DT API", productUnitId);

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/productunits/" + productUnitId);
                    return null;
                })
                .onSuccess(nothing -> log.debug("Product unit '{}' successfully deleted", productUnitId))
                .onFailure(error -> log.error("Unable to delete product unit '{}': {}", productUnitId,
                        error.getMessage(), error));
    }

    @Override
    public Try<List<ProductGtinDto>> getAllProductGtinsByProductUnitId(Integer productUnitId) {
        log.debug("Retrieving product gtins by product unit id '{}'", productUnitId);

        return Try.of(() -> graphQLWebClient.post(
                                GraphQLRequest.builder().resource("graphql/get-product-gtin-by-product-unit-id.graphql")
                                        .variables(Map.of("ProductUnitId", productUnitId))
                                        .build())
                        .map(graphQLResponse -> graphQLResponse.getFirstList(ProductGtinDto.class))
                        .map(productGtinDtos -> {
                            if (productGtinDtos.isEmpty())
                                throw new DigitalTwinNotFoundException("Product gtin with product unit id '" + productUnitId +
                                        "' does not exist");

                            return productGtinDtos;
                        })
                        .block())
                .onSuccess(productCharacteristicDto -> log.debug("Product gtins successfully retrieved: {}",
                        productCharacteristicDto))
                .onFailure(error -> log.error("Unable to retrieve product gtins descriptions : {}",
                        error.getMessage(), error));
    }

    @Override
    public Try<ProductGtinDto> createProductGtin(ProductGtinDto productGtinDto) {
        log.debug("Create product gtin '{}' via DT API", productGtinDto);

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/productgtins", productGtinDto,
                        ProductGtinDto.class))
                .onSuccess(createdProductGtinDto -> log.debug("Product unit successfully created: {}",
                        createdProductGtinDto))
                .onFailure(error -> log.error("Unable to create product unit '{}'", productGtinDto.getId()));
    }

    @Override
    public Try<Object> deleteProductGtin(Integer productGtinId) {
        log.debug("Deleting product gtin '{}' from DT API", productGtinId);

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/productgtins/" + productGtinId);
                    return null;
                })
                .onSuccess(nothing -> log.debug("Product unit '{}' successfully deleted", productGtinId))
                .onFailure(error -> log.error("Unable to delete product gtin '{}': {}", productGtinId,
                        error.getMessage(), error));
    }

    @Override
    public Try<List<ProductDescriptionDto>> getAllProductDescriptionsByProductId(String productId) {
        log.debug("Retrieving product descriptions by product id '{}'", productId);

        return Try.of(() -> graphQLWebClient.post(
                                GraphQLRequest.builder().resource("graphql/get-product-description-by-product-id" +
                                                ".graphql")
                                        .variables(Map.of("ProductId", productId))
                                        .build())
                        .map(graphQLResponse -> graphQLResponse.getFirstList(ProductDescriptionDto.class))
                        .block())
                .onSuccess(productCharacteristicDto -> log.debug("Product descriptions successfully retrieved: {}",
                        productCharacteristicDto))
                .onFailure(error -> log.error("Unable to retrieve product descriptions : {}", error.getMessage(),
                        error));
    }

    @Override
    public Try<ProductDescriptionDto> createProductDescription(ProductDescriptionDto productDescriptionDto) {
        log.debug("Create product description '{}' via DT API", productDescriptionDto);

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/productdescriptions", productDescriptionDto,
                        ProductDescriptionDto.class))
                .onSuccess(createdProductGtinDto -> log.debug("Product description successfully created: {}",
                        createdProductGtinDto))
                .onFailure(error -> log.error("Unable to create product description '{}'",
                        productDescriptionDto.getId()));
    }

    @Override
    public Try<Object> deleteProductDescription(Integer productDescriptionId) {
        log.debug("Deleting product description '{}' from DT API", productDescriptionId);

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/productdescriptions/" + productDescriptionId);
                    return null;
                })
                .onSuccess(nothing -> log.debug("Product description '{}' successfully deleted", productDescriptionId))
                .onFailure(error -> log.error("Unable to delete product description '{}': {}", productDescriptionId,
                        error.getMessage(), error));
    }

    @Override
    public Try<ProductPropertyDto> createProductProperty(ProductPropertyDto productPropertyDto) {
        log.debug("Create product property '{}' via DT API", productPropertyDto);

        return Try.of(() -> k4rRestTemplate.postForObject(apiUrl + "/products/" + productPropertyDto.getProductId()
                                + "/productproperties/" + productPropertyDto.getCharacteristicId(), productPropertyDto,
                        ProductPropertyDto.class))
                .onSuccess(createdProductGtinDto -> log.debug("Product property successfully created: {}",
                        createdProductGtinDto))
                .onFailure(error -> log.error("Unable to create product property '{}'", productPropertyDto.getId()));
    }

    @Override
    public Try<Object> deleteProductProperty(ProductPropertyDto productPropertyDto) {
        log.debug("Deleting product property '{}' from DT API", productPropertyDto.getId());

        return Try.of(() -> {
                    k4rRestTemplate.delete(apiUrl + "/products/" + productPropertyDto.getProductId() +
                            "/productproperties/" + productPropertyDto.getCharacteristicId());
                    return null;
                })
                .onSuccess(nothing -> log.debug("Product product property '{}' successfully deleted",
                        productPropertyDto.getId()))
                .onFailure(error -> log.error("Unable to delete product property '{}': {}", productPropertyDto.getId(),
                        error.getMessage(), error));
    }

    @Override
    public Try<Object> deleteProductProperty(String productId, Integer characteristicId) {
        return deleteProductProperty(ProductPropertyDto.builder()
                .productId(productId)
                .characteristicId(characteristicId)
                .build());
    }

    @Override
    public Try<List<ProductPropertyDto>> getAllProductPropertiesByProductId(String productId) {
        log.debug("Retrieving product characteristics for product '{}' from DT API", productId);

        return Try.of(() -> k4rRestTemplate.getForObject(apiUrl + "/products/" + productId + "/productproperties",
                        ProductPropertyDto[].class))
                .map(Arrays::asList)
                .onSuccess(productProperties -> log.debug("Product properties successfully retrieved: {}",
                        productProperties))
                .onFailure(error -> log.error("Unable to retrieve product '{}'", productId));
    }

    @Override
    public Try<ProductCharacteristicDto> getProductCharacteristicByCode(String code) {
        log.debug("Retrieving product characteristic by code '{}'", code);

        return Try.of(() -> graphQLWebClient.post(
                                GraphQLRequest.builder().resource("graphql/get-product-characteristic-by-code.graphql")
                                        .variables(Map.of("Code", code))
                                        .build())
                        .map(graphQLResponse -> graphQLResponse.getFirstList(ProductCharacteristicDto.class))
                        .map(productCharacteristicDtos -> {
                            if (productCharacteristicDtos.isEmpty())
                                throw new DigitalTwinNotFoundException("Product characteristic with code '" + code +
                                        "' does not exist");

                            return productCharacteristicDtos.get(0);
                        })
                        .block())
                .onSuccess(productCharacteristicDto -> log.debug("Product characteristic successfully retrieved: {}",
                        productCharacteristicDto))
                .onFailure(error -> log.error("Unable to retrieve product characteristic : {}", error.getMessage(),
                        error));
    }

    @Override
    public Try<List<Product>> getAllProducts() {
        log.debug("Retrieving all products from DT API");

        return Try.of(() -> k4rRestTemplate.getForObject(apiUrl + "/products", Product[].class))
                .map(Arrays::asList)
                .onSuccess(products -> log.debug("Products successfully retrieved: {}", products))
                .onFailure((error) -> log.error("Unable to retrieve products from DT API: {}", error.getMessage(),
                        error));
    }
}
