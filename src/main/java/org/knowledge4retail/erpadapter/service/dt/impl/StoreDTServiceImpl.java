package org.knowledge4retail.erpadapter.service.dt.impl;

import graphql.kickstart.spring.webclient.boot.GraphQLRequest;
import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.StoreDto;
import org.knowledge4retail.erpadapter.dto.dt.product.ProductCharacteristicDto;
import org.knowledge4retail.erpadapter.exception.DigitalTwinNotFoundException;
import org.knowledge4retail.erpadapter.service.dt.StoreDTService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class StoreDTServiceImpl implements StoreDTService {

    private static final String SERVICE_PATH = "/stores";
    private final RestTemplate restTemplateK4R;
    private final GraphQLWebClient graphQLWebClient;

    @Value("${org.knowledge4retail.dt.baseUrl}")
    private String apiUrl;

    public StoreDTServiceImpl(RestTemplate restTemplateK4R, GraphQLWebClient graphQLWebClient) {
        this.restTemplateK4R = restTemplateK4R;
        this.graphQLWebClient = graphQLWebClient;
    }

    @Override
    public StoreDto read(int id) {
        return restTemplateK4R.getForObject(apiUrl + "/" + SERVICE_PATH + "/" + id, StoreDto.class);
    }

    @Override
    public List<StoreDto> readAll() {
        ResponseEntity<StoreDto[]> response = restTemplateK4R.getForEntity(apiUrl + "/" + SERVICE_PATH,
                StoreDto[].class);

        return Arrays.asList(response.getBody());
    }

    @Override
    public Try<StoreDto> create(StoreDto storeDto) {
        return Try.of(() -> {
            log.info(String.format("Creating digital twin for store \"%s\"", storeDto.getStoreNumber()));

            HttpEntity<StoreDto> requestEntity = new HttpEntity<>(storeDto);
            ResponseEntity<StoreDto> responseEntity = restTemplateK4R.exchange(apiUrl + "/" + SERVICE_PATH,
                    HttpMethod.POST, requestEntity, StoreDto.class);

            return responseEntity.getBody();
        });
    }

    @Override
    public Try<StoreDto> update(int internalStoreId, StoreDto storeDto) {
        return Try.of(() -> {
            log.info(String.format("Updating digital twin (internal id %d) of store %s", internalStoreId,
                    storeDto.getStoreNumber()));

            HttpEntity<StoreDto> requestEntity = new HttpEntity<>(storeDto);
            ResponseEntity<StoreDto> response =
                    restTemplateK4R.exchange(apiUrl + "/" + SERVICE_PATH + "/" + internalStoreId, HttpMethod.PUT,
                            requestEntity, StoreDto.class);

            return response.getBody();
        });
    }

    @Override
    public Try<Void> delete(int internalStoreId) {
        return Try.of(() -> {
            log.info(String.format("Deleting digital twin of store %d (internal id)", internalStoreId));

            restTemplateK4R.delete(apiUrl + "/" + SERVICE_PATH + "/" + internalStoreId, HttpMethod.DELETE);

            return null;
        });
    }

    @Override
    public Try<Integer> getStoreIdByStoreNumber(String storeNumber) {
        log.debug("Retrieving store id by store number '{}'", storeNumber);

        return Try.of(() -> graphQLWebClient.post(
                                GraphQLRequest.builder().resource("graphql/get-store-id-by-store-number.graphql")
                                        .variables(Map.of("Number", storeNumber))
                                        .build())
                        .map(graphQLResponse -> graphQLResponse.getFirstList(StoreDto.class))
                        .map(storeDtos -> {
                            if (storeDtos.isEmpty())
                                throw new DigitalTwinNotFoundException("Store with store number '" + storeNumber +
                                        "' does not exist");

                            return storeDtos.get(0).getId();
                        })
                        .block())
                .onSuccess(storeId -> log.debug("Store id successfully retrieved: {}",
                        storeId))
                .onFailure(error -> log.error("Unable to retrieve store id: {}", error.getMessage(), error));
    }
}
