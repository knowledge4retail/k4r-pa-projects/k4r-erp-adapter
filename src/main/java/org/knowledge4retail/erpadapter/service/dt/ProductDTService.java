package org.knowledge4retail.erpadapter.service.dt;

import io.vavr.control.Try;
import org.knowledge4retail.erpadapter.dto.dt.product.*;
import org.knowledge4retail.erpadapter.dto.product.Product;

import java.util.List;

public interface ProductDTService {

    Try<ProductDto> getProduct(String productId);

    Try<ProductDto> createProduct(ProductDto productDto);

    Try<Object> deleteProduct(String productId);

    Try<MaterialGroupDto> getMaterialGroupByName(String name);

    Try<UnitDto> getUnitBySymbol(String symbol);

    Try<List<ProductUnitDto>> getAllProductUnitsByProductId(String productId);

    Try<ProductUnitDto> createProductUnit(ProductUnitDto productUnitDto);

    Try<Object> deleteProductUnit(Integer productUnitId);

    Try<List<ProductGtinDto>> getAllProductGtinsByProductUnitId(Integer productUnitId);

    Try<ProductGtinDto> createProductGtin(ProductGtinDto productGtinDto);

    Try<Object> deleteProductGtin(Integer productGtinId);

    Try<List<ProductDescriptionDto>> getAllProductDescriptionsByProductId(String productId);

    Try<ProductDescriptionDto> createProductDescription(ProductDescriptionDto productDescriptionDto);

    Try<Object> deleteProductDescription(Integer productDescriptionId);

    Try<ProductPropertyDto> createProductProperty(ProductPropertyDto productPropertyDto);

    Try<Object> deleteProductProperty(ProductPropertyDto productPropertyDto);

    Try<Object> deleteProductProperty(String productId, Integer characteristicId);

    Try<List<ProductPropertyDto>> getAllProductPropertiesByProductId(String productId);

    Try<ProductCharacteristicDto> getProductCharacteristicByCode(String code);

    Try<List<Product>> getAllProducts();

}
