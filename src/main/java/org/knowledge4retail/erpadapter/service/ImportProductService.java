package org.knowledge4retail.erpadapter.service;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.BasicDto;
import org.knowledge4retail.erpadapter.dto.dataimport.*;
import org.knowledge4retail.erpadapter.dto.dt.product.*;
import org.knowledge4retail.erpadapter.dto.product.Product;
import org.knowledge4retail.erpadapter.exception.DigitalTwinModificationException;
import org.knowledge4retail.erpadapter.exception.DigitalTwinNotFoundException;
import org.knowledge4retail.erpadapter.operation.DigitalTwinOperation;
import org.knowledge4retail.erpadapter.operation.product.create.*;
import org.knowledge4retail.erpadapter.operation.product.delete.*;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.knowledge4retail.erpadapter.service.dt.ProductDTService;
import org.knowledge4retail.erpadapter.service.dt.StoreDTService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("importProductService")
public class ImportProductService extends ImportService<ImportProductRequestDto> {

    private final LogicalSystemService logicalSystemService;
    private final ProductDTService productDTService;
    private final StoreDTService storeDTService;

    public ImportProductService(LogicalSystemService logicalSystemService, ProductDTService productDTService,
                                StoreDTService storeDTService) {
        this.logicalSystemService = logicalSystemService;
        this.productDTService = productDTService;
        this.storeDTService = storeDTService;
    }

    @Override
    public ImportOperationResult importData(String logicalSystemId, ImportProductRequestDto data) {
        if (!logicalSystemService.exists(logicalSystemId)) {
            throw new EntityNotFoundException("A logical system with id "
                    + "'" + logicalSystemId + "'"
                    + " does not exist. No products were imported.");
        }

        List<ImportResult> importResults = data.getProducts()
                .parallelStream()
                .map(this::importProduct)
                .collect(Collectors.toList());

        return computeResult(importResults, data.getProducts().size());
    }

    @Override
    public ImportOperationResult deleteData(String logicalSystemId, List<String> idList) {
        if (!logicalSystemService.exists(logicalSystemId)) {
            throw new EntityNotFoundException("A logical system with id "
                    + "'" + logicalSystemId + "'"
                    + " does not exist. No products were imported.");
        }

        List<ImportResult> importResults = idList
                .parallelStream()
                .map(this::deleteProduct)
                .collect(Collectors.toList());

        return computeResult(importResults, idList.size());
    }

    private ImportResult importProduct(Product product) {
        List<DigitalTwinOperation<?, ?>> executedOperations = new ArrayList<>();

        ImportResult importResult = new ImportResult();
        importResult.setEntityId(product.getProductID());
        importResult.setStatus(ImportResult.Status.SUCCESSFUL);

        try {
            MaterialGroupDto materialGroupDto = new CreateMaterialGroupOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(product);

            ProductDto productDto = new CreateProductOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(product, materialGroupDto.getId());

            new CreateProductDescriptionOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(product);

            new CreateProductPropertyOperation(productDTService, storeDTService)
                    .addToExecutionList(executedOperations)
                    .execute(product);

            List<ProductUnitDto> productUnits = new CreateProductUnitOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(product, productDto.getId());

            new CreateProductGtinOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(productUnits);

        } catch (DigitalTwinModificationException ex) {
            log.warn("Exception thrown while importing product '{}'. Rolling back product now...",
                    product.getProductID(), ex);

            FailedImportResult failedImportResult = new FailedImportResult();
            failedImportResult.setId(product.getProductID());
            failedImportResult.setMessage(ex.getMessage());

            // Loop through list in reverse in order to avoid fk key constraints while deleting entities...
            Collections.reverse(executedOperations);
            for (DigitalTwinOperation<?, ?> operation : executedOperations) {
                try {
                    operation.rollback();
                } catch (DigitalTwinModificationException exception) {
                    log.error("Unable to fully rollback product '{}': {}", exception.getDtEntityKey(),
                            exception.getMessage());

                    failedImportResult.setSuccessfulRollback(false);
                    failedImportResult.getFailedRollbackResults().add(FailedRollbackResult.builder()
                            .entityKey(exception.getDtEntityKey())
                            .id(product.getProductID())
                            .type(exception.getType())
                            .reason(exception.getMessage())
                            .build());
                }
            }

            log.info("Rollback of product '{}' complete.", product.getProductID());
            failedImportResult.setSuccessfulRollback(true);

            importResult.setFailedImportResult(failedImportResult);
            importResult.setStatus(ImportResult.Status.FAILED);
        }

        return importResult;
    }

    private ImportResult deleteProduct(String productId) {
        List<DigitalTwinOperation<?, ?>> executedOperations = new ArrayList<>();

        ImportResult importResult = new ImportResult();
        importResult.setEntityId(productId);
        importResult.setStatus(ImportResult.Status.SUCCESSFUL);

        try {

            //TODO: Do this with cross reference tables instead of resolving keys by querying DT API
            Map<Class<? extends BasicDto>, List<Integer>> idsToDelete = resolveIds(productId);

            new DeleteProductGtinOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(idsToDelete.get(ProductGtinDto.class));

            new DeleteProductUnitOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(idsToDelete.get(ProductUnitDto.class));

            new DeletePropertyOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(productId, idsToDelete.get(ProductCharacteristicDto.class));

            new DeleteProductDescriptionOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(idsToDelete.get(ProductDescriptionDto.class));

            new DeleteProductOperation(productDTService)
                    .addToExecutionList(executedOperations)
                    .execute(productId);

        } catch (Exception ex) {
            log.warn("Exception thrown while deleting product '{}'", productId, ex);

            FailedImportResult failedImportResult = new FailedImportResult();
            failedImportResult.setId(productId);
            failedImportResult.setMessage(ex.getMessage());

            importResult.setStatus(ImportResult.Status.FAILED);
            importResult.setFailedImportResult(failedImportResult);
        }

        return importResult;
    }

    private Map<Class<? extends BasicDto>, List<Integer>> resolveIds(String productId) {
        Map<Class<? extends BasicDto>, List<Integer>> idMap = new HashMap();

        ProductDto productDto = productDTService.getProduct(productId)
                .getOrElseThrow(DigitalTwinNotFoundException::new);

        // Product Descriptions
        List<Integer> productDescriptionIds = productDTService.getAllProductDescriptionsByProductId(productId)
                .get()
                .stream()
                .map(ProductDescriptionDto::getId)
                .collect(Collectors.toList());
        idMap.put(ProductDescriptionDto.class, productDescriptionIds);

        //Product Properties/Characteristics
        List<Integer> characteristicIds = productDTService.getAllProductPropertiesByProductId(productId)
                .get()
                .stream()
                .map(ProductPropertyDto::getCharacteristicId)
                .collect(Collectors.toList());
        idMap.put(ProductCharacteristicDto.class, characteristicIds);

        //Product Unit
        List<Integer> productUnitIds =
                productDTService.getAllProductUnitsByProductId(productId).get()
                .stream()
                .map(ProductUnitDto::getId)
                .collect(Collectors.toList());
        idMap.put(ProductUnitDto.class, productUnitIds);

        //Product Gtins
        List<Integer> productGtinIds = productUnitIds.stream()
                .map(productUnitId -> productDTService.getAllProductGtinsByProductUnitId(productUnitId)
                        .get())
                .flatMap(List::stream)
                .map(ProductGtinDto::getId)
                .collect(Collectors.toList());
        idMap.put(ProductGtinDto.class, productGtinIds);

        return idMap;
    }
}
