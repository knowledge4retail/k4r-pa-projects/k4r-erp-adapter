package org.knowledge4retail.erpadapter.service;

import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.dataimport.FailedImportResult;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportOperationResult;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportResult;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Generic interface for data import.
 *
 * @param <T> Type of data to be imported
 */
@Slf4j
public abstract class ImportService<T> {

    /**
     * Imports data from a given logical system.
     *
     * @param logicalSystemId id of logical system
     * @param data data to be imported
     * @return {@link ImportOperationResult} result of import
     */
   public abstract ImportOperationResult importData(String logicalSystemId, T data);

   public abstract ImportOperationResult deleteData(String logicalSystemId, List<String> idList);

    /**
     * Computes and returns the total import result of based off a list imports.
     *
     * @param importResults list of import steps
     * @param totalCount number of total imports
     * @return total result of import
     */
    protected ImportOperationResult computeResult(List<ImportResult> importResults, int totalCount) {
        long successfulOperationsCount = importResults.stream()
                .filter(importResult -> importResult.getStatus() == ImportResult.Status.SUCCESSFUL)
                .count();
        long failedOperationsCount = importResults.stream()
                .filter(importResult -> importResult.getStatus() == ImportResult.Status.FAILED)
                .count();
        long successfulRollbackCount = importResults.stream()
                .filter(importResult -> importResult.getStatus() == ImportResult.Status.FAILED &&
                        importResult.getFailedImportResult().isSuccessfulRollback())
                .count();
        long failedRollbackCount = importResults.stream()
                .filter(importResult -> importResult.getStatus() == ImportResult.Status.FAILED)
                .map(ImportResult::getFailedImportResult)
                .filter(Objects::nonNull)
                .filter(failedImportResult -> !failedImportResult.getFailedRollbackResults().isEmpty())
                .map(failedImportResult -> failedImportResult.getFailedRollbackResults().size())
                .count();

        List<FailedImportResult> failedImports = importResults.stream()
                .map(ImportResult::getFailedImportResult)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        ImportOperationResult.ImportStatus status;
        if(successfulOperationsCount == totalCount) {
            status = ImportOperationResult.ImportStatus.SUCCESSFUL;
        } else if (failedOperationsCount == totalCount) {
            status = ImportOperationResult.ImportStatus.FAILED;
        } else {
            status = ImportOperationResult.ImportStatus.COMPLETE_WITH_ERRORS;
        }

        return ImportOperationResult.builder()
                .completionDate(new Date())
                .totalOperationsCount(totalCount)
                .successfulOperationsCount(successfulOperationsCount)
                .failedOperationsCount(failedOperationsCount)
                .successfulRollbackCount(successfulRollbackCount)
                .failedRollbackCount(failedRollbackCount)
                .status(status)
                .failedImports(failedImports)
                .build();
    }
}
