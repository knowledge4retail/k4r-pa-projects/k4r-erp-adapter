package org.knowledge4retail.erpadapter.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import graphql.kickstart.spring.webclient.boot.GraphQLWebClient;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;

@Configuration
public class K4RRestTemplateConfig {

    @Value("${keystore.enabled}")
    private boolean keystoreEnabled;

    @Value("${keystore.path}")
    private String keystorePath;

    @Value("${keystore.passphrase}")
    private String keystorePassphrase;

    @Value("${org.knowledge4retail.dt.graphqlUrl}")
    private String graphQlBaseUrl;

    @Bean
    public SimpleDateFormat simpleDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    }

    @Bean
    public Gson gson() {
        return new GsonBuilder().setDateFormat(simpleDateFormat().toPattern()).create();
    }

    @Bean
    public RestTemplate k4rRestTemplate() throws KeyStoreException, IOException, CertificateException,
            NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DigitalTwinRestTemplateErrorHandler(gson()));

        if (keystoreEnabled) {
            SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext());
            CloseableHttpClient httpClient = HttpClients.custom()
                    .setSSLSocketFactory(sslConnectionSocketFactory)
                    .build();

            restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
        }

        return restTemplate;
    }

    @Bean
    public GraphQLWebClient graphQLWebClient() {
        ObjectMapper objectMapper = new ObjectMapper();

        WebClient webClient;
        if (keystoreEnabled) {
            HttpClient httpClient = HttpClient.create().secure(sslSpec -> {
                try {
                    sslSpec.sslContext(getWebClientSSLContext());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });

            ClientHttpConnector clientHttpConnector = new ReactorClientHttpConnector(httpClient);
            webClient = WebClient.builder()
                    .baseUrl(graphQlBaseUrl)
                    .clientConnector(clientHttpConnector)
                    .build();
        } else {
            webClient = WebClient.builder()
                    .baseUrl(graphQlBaseUrl)
                    .build();
        }

        return GraphQLWebClient.newInstance(webClient, objectMapper);
    }

    private SSLContext sslContext() throws KeyStoreException, CertificateException, IOException,
            NoSuchAlgorithmException, UnrecoverableKeyException, KeyManagementException {
        KeyStore clientStore = getKeyStore();

        SSLContextBuilder sslContextBuilder = new SSLContextBuilder();
        sslContextBuilder.setProtocol("TLS");
        sslContextBuilder.loadKeyMaterial(clientStore, keystorePassphrase.toCharArray());
        sslContextBuilder.loadTrustMaterial(new TrustSelfSignedStrategy());

        return sslContextBuilder.build();
    }

    private KeyStore getKeyStore() throws KeyStoreException, IOException, NoSuchAlgorithmException,
            CertificateException {
        KeyStore clientStore = KeyStore.getInstance("PKCS12");

        Resource resource = new FileSystemResource(keystorePath);
        if (resource.exists()) {
            try (InputStream inputStream = resource.getInputStream()) {
                clientStore.load(inputStream, keystorePassphrase.toCharArray());
            }
        } else {
            throw new RuntimeException("Cannot find resource: " + resource.getFilename());
        }
        return clientStore;
    }

    public SslContext getWebClientSSLContext() throws CertificateException, KeyStoreException, IOException,
            NoSuchAlgorithmException, UnrecoverableKeyException {

        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(getKeyStore(), keystorePassphrase.toCharArray());

        return SslContextBuilder.forClient()
                .keyManager(keyManagerFactory)
                .trustManager(InsecureTrustManagerFactory.INSTANCE)
                .build();
    }
}
