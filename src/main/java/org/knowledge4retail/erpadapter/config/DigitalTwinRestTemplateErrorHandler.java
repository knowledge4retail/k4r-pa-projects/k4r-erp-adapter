package org.knowledge4retail.erpadapter.config;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.knowledge4retail.erpadapter.dto.DTApiError;
import org.knowledge4retail.erpadapter.exception.DigitalTwinApiException;
import org.knowledge4retail.erpadapter.exception.DigitalTwinNotFoundException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.nio.charset.Charset;

import static org.springframework.http.HttpStatus.Series.CLIENT_ERROR;
import static org.springframework.http.HttpStatus.Series.SERVER_ERROR;

@Slf4j
@Component
public class DigitalTwinRestTemplateErrorHandler implements ResponseErrorHandler {

    private final Gson gson;

    public DigitalTwinRestTemplateErrorHandler(Gson gson) {
        this.gson = gson;
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return (
                response.getStatusCode().series() == CLIENT_ERROR
                        || response.getStatusCode().series() == SERVER_ERROR);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        String responseBody = StreamUtils.copyToString(response.getBody(), Charset.defaultCharset());
        log.warn("Digital Twin API request failed (status={}). Response: {}", response.getStatusCode().value(),
                responseBody);

        if(response.getStatusCode().value() == 404) {
            DTApiError apiError = gson.fromJson(responseBody, DTApiError.class);

            throw new DigitalTwinNotFoundException("Digital Twin not found: " + apiError.getMessage());
        }

        throw new DigitalTwinApiException("Digital Twin API request failed. HTTP status " +
                response.getStatusCode().value() + ". API Response: " + responseBody);
    }
}