CREATE TABLE adapter_activation
(
    store_id          VARCHAR(255) NOT NULL,
    logical_system_id VARCHAR(255),
    is_active         BOOLEAN,
    CONSTRAINT pk_adapter_activation PRIMARY KEY (store_id)
);

CREATE TABLE adapter_object
(
    id VARCHAR(255) NOT NULL,
    CONSTRAINT pk_adapter_object PRIMARY KEY (id)
);

CREATE TABLE adapter_object_text
(
    object_name           VARCHAR(255),
    description           VARCHAR(255),
    adapter_object_id     VARCHAR(255) NOT NULL,
    language              VARCHAR(255) NOT NULL,
    adapter_object_textpk VARCHAR(255),
    CONSTRAINT pk_adapter_object_text PRIMARY KEY (adapter_object_id, language)
);

CREATE TABLE customer_ext_id
(
    internal_customer_id VARCHAR(255),
    logical_system_id    VARCHAR(255) NOT NULL,
    external_customer_id VARCHAR(255) NOT NULL,
    CONSTRAINT pk_customer_ext_id PRIMARY KEY (logical_system_id, external_customer_id)
);

CREATE TABLE logical_system
(
    logical_system_id VARCHAR(255) NOT NULL,
    adapter_host      VARCHAR(255),
    system_type_id    VARCHAR(255) NOT NULL,
    CONSTRAINT pk_logical_system PRIMARY KEY (logical_system_id)
);

CREATE TABLE object_activation
(
    logical_system_logical_system_id VARCHAR(255),
    is_active                        BOOLEAN,
    id_generation                    VARCHAR(255),
    object_id                        VARCHAR(255) NOT NULL,
    logical_system_id                VARCHAR(255) NOT NULL,
    CONSTRAINT pk_object_activation PRIMARY KEY (object_id, logical_system_id)
);

CREATE TABLE product_ext_id
(
    internal_product_id VARCHAR(255),
    logical_system_id   VARCHAR(255) NOT NULL,
    external_product_id VARCHAR(255) NOT NULL,
    CONSTRAINT pk_product_ext_id PRIMARY KEY (logical_system_id, external_product_id)
);

CREATE TABLE store_ext_id
(
    internal_store_id INTEGER,
    logical_system_id VARCHAR(255) NOT NULL,
    external_store_id VARCHAR(255) NOT NULL,
    CONSTRAINT pk_store_ext_id PRIMARY KEY (logical_system_id, external_store_id)
);

CREATE TABLE system_type
(
    id                     VARCHAR(255) NOT NULL,
    name                   VARCHAR(255),
    description            VARCHAR(255),
    adapter_implementation VARCHAR(255),
    CONSTRAINT pk_system_type PRIMARY KEY (id)
);

ALTER TABLE adapter_activation
    ADD CONSTRAINT FK_ADAPTER_ACTIVATION_ON_LOGICAL_SYSTEM FOREIGN KEY (logical_system_id) REFERENCES logical_system (logical_system_id);

ALTER TABLE adapter_object_text
    ADD CONSTRAINT FK_ADAPTER_OBJECT_TEXT_ON_ADAPTEROBJECTTEXTPK FOREIGN KEY (adapter_object_textpk) REFERENCES adapter_object (id);

ALTER TABLE object_activation
    ADD CONSTRAINT FK_OBJECT_ACTIVATION_ON_LOGICALSYSTEM_LOGICAL_SYSTEM FOREIGN KEY (logical_system_logical_system_id) REFERENCES logical_system (logical_system_id);