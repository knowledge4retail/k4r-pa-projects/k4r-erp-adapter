package org.knowledge4retail.erpadapter.integration;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.knowledge4retail.erpadapter.dto.LogicalSystemDto;
import org.knowledge4retail.erpadapter.dto.SystemTypeDto;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.knowledge4retail.erpadapter.service.db.SystemTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class SystemIntegrationTest {

    private static final String SYSTEM_API_URI = "/api/v0/system";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private LogicalSystemService logicalSystemService;

    @Autowired
    private SystemTypeService systemTypeService;

    @Before
    public void setUpTestDB() {
        systemTypeService.create(SystemTypeDto.builder()
                .id("ERP")
                .name("S/4 Hana")
                .description("Sample S/4 Hana")
                .adapterImplementation("SAP")
                .build());

        logicalSystemService.create(LogicalSystemDto.builder()
                .logicalSystemId("RDE")
                .hostName("localhost:8080")
                .systemTypeId("S4H")
                .build());
    }

    @Test
    public void getAllLogicalSystemsReturns200Test() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get(SYSTEM_API_URI + "/logical")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


    @Test
    public void createLogicalSystemReturns201Test() throws Exception {
        LogicalSystemDto payload = LogicalSystemDto.builder()
                .logicalSystemId("ERP1")
                .systemTypeId("ERP")
                .hostName("localhost:8080")
                .build();

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYSTEM_API_URI + "/logical")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(payload))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void createLogicalSystemWithDuplicateIdReturns400Test() throws Exception {
        LogicalSystemDto payload = LogicalSystemDto.builder()
                .logicalSystemId("ERP1")
                .systemTypeId("ERP")
                .hostName("localhost:8080")
                .build();

        logicalSystemService.create(payload);

        mockMvc.perform(MockMvcRequestBuilders
                .post(SYSTEM_API_URI + "/logical")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new Gson().toJson(payload))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}
