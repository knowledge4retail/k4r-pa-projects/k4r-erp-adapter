package org.knowledge4retail.erpadapter.integration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.knowledge4retail.erpadapter.dto.LogicalSystemDto;
import org.knowledge4retail.erpadapter.dto.StoreDto;
import org.knowledge4retail.erpadapter.dto.SystemTypeDto;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportLine;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportStoreRequestDto;
import org.knowledge4retail.erpadapter.dto.dataimport.ImportOperationResult;
import org.knowledge4retail.erpadapter.service.db.LogicalSystemService;
import org.knowledge4retail.erpadapter.service.db.StoreExternalIdService;
import org.knowledge4retail.erpadapter.service.db.SystemTypeService;
import org.knowledge4retail.erpadapter.service.dt.StoreDTService;
import org.knowledge4retail.erpadapter.service.ImportStoreService;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * TODO: Add import controller to integration test
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ImportServiceTest {

    @MockBean
    private StoreDTService storeDTService;

    @Autowired
    private StoreExternalIdService storeExternalIdService;

    @Autowired
    private LogicalSystemService logicalSystemService;

    @Autowired
    private SystemTypeService systemTypeService;

    @Before
    public void setUpTestDB() {
        systemTypeService.create(SystemTypeDto.builder()
                .id("S4H")
                .name("S/4 Hana")
                .description("Sample S/4 Hana")
                .adapterImplementation("SAP")
                .build());

        logicalSystemService.create(LogicalSystemDto.builder()
                .logicalSystemId("RDE")
                .hostName("localhost:8080")
                .systemTypeId("S4H")
                .build());
    }

    @After
    public void clearTestDB() {
        systemTypeService.deleteAll();
        logicalSystemService.deleteAll();
        storeExternalIdService.deleteAll();
    }

    @Test
    public void returnListOfCreatedStoresWhenImported() {
        //Return the store passed as argument and set Id to 1
        Mockito.when(storeDTService.create(Mockito.any(StoreDto.class)))
                .then(invocationOnMock -> {
                    StoreDto storePassedAsArgument = (StoreDto) invocationOnMock.getArguments()[0];
                    storePassedAsArgument.setId(1);

                    return storePassedAsArgument;
                });

        ImportStoreService importStoreService = new ImportStoreService(storeDTService, storeExternalIdService,
                logicalSystemService);
        ImportOperationResult responseDto = importStoreService.importData("RDE", createStoreRequest());

        assertEquals(1, responseDto.getSuccessfulOperationsCount());
        assertEquals(1, storeExternalIdService.readAll().size());
    }

    @Test
    public void returnListOfManyCreatedStoreWhenImported() {
        StoreDto store2 = createMockStoreDto();
        store2.setStoreNumber("YLB2");

        ImportStoreRequestDto storeRequest = createStoreRequest();
        storeRequest.getData().add(ImportLine.<StoreDto>builder().operationCode("C").payload(store2).build());

        var ref = new Object() {
            int returnStoreId = 1;
        };

        Mockito.when(storeDTService.create(Mockito.any(StoreDto.class)))
                .then(invocationOnMock -> {
                    StoreDto storePassedAsArgument = (StoreDto) invocationOnMock.getArguments()[0];
                    storePassedAsArgument.setId(ref.returnStoreId);

                    ref.returnStoreId++;

                    return storePassedAsArgument;
                });

        ImportStoreService importStoreService = new ImportStoreService(storeDTService, storeExternalIdService,
                logicalSystemService);
        ImportOperationResult responseDto = importStoreService.importData("RDE", storeRequest);

        assertEquals(2, storeExternalIdService.readAll().size());
    }

    @Test(expected = EntityNotFoundException.class)
    public void returnEntityNotFoundExceptionWhenImportUnknownStoreId() {
        ImportStoreService importStoreService = new ImportStoreService(storeDTService, storeExternalIdService,
                logicalSystemService);
        importStoreService.importData("UNKNOWN", createStoreRequest());
    }

    @Test(expected = IllegalArgumentException.class)
    public void returnIllegalArgumentExceptionWhenImportWithUnknownOperationType() {
        ImportStoreRequestDto storeRequest = createStoreRequest();
        storeRequest.getData().get(0).setOperationCode("UNKNOWN");

        ImportStoreService importStoreService = new ImportStoreService(storeDTService, storeExternalIdService,
                logicalSystemService);
        importStoreService.importData("RDE", storeRequest);
    }

    private StoreDto createMockStoreDto() {
        return StoreDto.builder()
                .addressCity("Bremen").addressCountry("DE").addressPostcode("28359").addressStreet("Bibiotheksstr. 1").storeName("Lab Bremen")
                .storeNumber("YLB1").build();
    }

    private ImportStoreRequestDto createStoreRequest() {
        ImportLine<StoreDto> c =
                ImportLine.<StoreDto>builder().operationCode("C").payload(createMockStoreDto()).build();
        List<ImportLine<StoreDto>> storeImportLines = new ArrayList();
        storeImportLines.add(c);


        return ImportStoreRequestDto.builder().data(storeImportLines).build();
    }
}
