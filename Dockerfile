FROM maven:3.6.3-jdk-11 as MAVEN_TOOL_CHAIN

WORKDIR /build
#COPY ./ /build
COPY ./src/ /build/src/
COPY ./pom.xml /build
RUN mvn clean install -P docker -DskipTests=true

FROM openjdk:11.0.6-jre
COPY --from=MAVEN_TOOL_CHAIN /build/target/k4r-erp-adapter.jar /usr/app/app.jar
ENTRYPOINT ["java","-jar","/usr/app/app.jar"]