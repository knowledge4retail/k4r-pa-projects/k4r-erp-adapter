# K4R ERP Adapter
Um die K4R-Plattform mit einem oder mehreren ERP Systemen zu integrieren wird ein generischer ERP Adapter definiert,
der die spezifischen ERP Adapter Implementierungen gegenüber der Plattform maskiert. Außerdem übernimmt der generische 
ERP Adapter die Aufgabe die jeweiligen Objektschlüssel zwischen Platform und angeschlossenem System zu matchen.
Hierfür werden entsprechende Cross-Reference Tabellen verwendet.

## Architektur

![Image_of_Adapter_Architecture](images/img.png)

## Funktionsweise
Grundlegende Annahmen:

- Jede Filiale kann einem angeschlossenen System zugeordnet werden, kann aber in unterschiedlichen angeschlossenen Systemen existieren - Buchungen zur Filiale erfolgen in genau einem System, die Filiale kann aber in Prozessen aus anderen Systemen verwendet werden (z.B. Umlagerung)
- Ein K4R-Artikel kann in unterschiedlichen angeschlossenen Systemen mit unterschiedlichen IDs existieren

Nummernvergabe und Cross reference Tables:

Da unterschiedliche Quellsysteme angebunden werden können, die die gleichen Objekte übermitteln muss bei der Nummernvergabe entschieden werden wie die Objekt-ID vergeben wird. Hierfür kommen im Prinzip drei Varianten in Frage:

- Nummer aus Quellsystem übernehmen
- Nummer über Funktion ableiten
- Nummer intern vergeben (DT API vergibt interne ID)

Eingehende und ausgehende Nachrichten werden dann über die Cross-Reference Tabellen in den jeweiligen Zielschlüssel umgesetzt.

## Entwicklungsumgebung
tbd: (docker-compose)




